% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
% Copyright (C) 2008-2009 - Consortium Scilab - Digiteo - Vincent Couvert
% Copyright (C) 2008-2009 - INRIA - Serge Steer
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%% Chapter 2
\chapter{Quadratic optimization}


In this chapter, we present the \scifun{qpsolve}, \scifun{qp\_solve} 
and \scifun{qld} Scilab functions, which 
allow to solve linear-quadratic optimization problems. 
We especially analyze the management of the linear 
algebra as well as the memory requirements of these solvers.
We give examples which show how to use these functions.
In the final section, we present some guidelines to choose a 
linear-quadratic optimization solver.

The functions provided by Scilab for quadratic optimization are presented 
in the figure \ref{fig-scilab-quaoptfuns}.

\begin{figure}
\begin{center}
\begin{tabular}{|l|l|}
\hline
\scifun{qld} & linear quadratic programming solver\\
\scifun{qp\_solve} & linear quadratic programming solver\\
\scifun{qpsolve} & linear quadratic programming solver\\
\hline
Quapro Toolbox &\\
\scifun{linpro} & linear programming solver\\
\scifun{quapro} & linear quadratic programming solver\\
\scifun{mps2linpro} & convert lp problem given in MPS format to linpro format\\
\hline
\end{tabular}
\end{center}
\caption{Functions for quadratic optimization.}
\label{fig-scilab-quaoptfuns}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The function that we are going to present are designed to be a replacement 
for the former Quapro solver.
This solver has been transformed into a Scilab toolbox for 
licence reasons. 
The Quapro toolbox is presented in the chapter \ref{chapter-quapro}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The \scifun{qp\_solve} and \scifun{qpsolve} functions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Mathematical point of view}
This kind of optimization is the minimization of function 
$f(x)$ with 
$$f(x) = \frac{1}{2} x^T Q x - p^T x$$ under the constraints :
\begin{eqnarray}
C_1^T x &=& b_1 \\
C_2^T x &\geq& b_2 \\
\end{eqnarray}

We notice that the linear part of the objective function, 
associated with the $p$ variable, has a minus sign.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{qpsolve}

The Scilab function \scifun{qpsolve} is a solver for quadratic problems 
when $Q$ is symmetric positive definite. 
The constraints matrix can be dense or sparse.

The \scifun{qpsolve} function is a Scilab macro which aims 
at providing the same interface (that is, the same input/output arguments) as 
the quapro solver. 

The \scifun{qpsolve} Scilab macro is based on the work by Berwin A Turlach 
from The University of Western Australia, Crawley \cite{quadprogTurlach}. The solver is 
implemented in Fortran 77. This routine uses the Goldfarb/Idnani 
algorithm \cite{GoldfarbIdnani1982,GoldfarbIdnani1983}.


The \scifun{qpsolve} macro calls the \scifun{qp\_solve} compiled primitive. 
The internal source code for \scifun{qpsolve} manages the equality and 
inequality constraints so that it can be processed by the \scifun{qp\_solve} primitive. 

For more details about this solver, please refer to 
\href{http://www.scilab.org/product/man/qpsolve.html}{
\underline{Scilab online help for \scifun{qpsolve}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{qp\_solve}

The \scifun{qp\_solve} compiled primitive is an interface for the 
fortran 77 solver. The interface is implemented in the C source code sci\_qp\_solve.c.
Depending on the constraints matrix, a dense or sparse solver is used :
\begin{itemize}
\item If the constraints matrix $C$ is dense, the qpgen2 fortran 77 routine is used. 
The qpgen2 routine is the original, unmodified algorithm which was written by 
Turlach (the original name was solve.QP.f)
\item If the constraints matrix $C$ is a Scilab sparse matrix, 
the qpgen1sci routine is called. This routine is a modification of 
the original routine qpgen1, in order to adapt to the specific Scilab sparse matrices storage. 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Memory requirements}

Suppose that $n$ is the dimension of the quadratic matrix $Q$ and $m$ is 
the sum of the number of equality constraints $me$ and inequality 
constraints $md$. Then, the temporary work array which is allocated in the 
primitive has the size
\begin{eqnarray}
r &=& \min(n,m),\\
lw &=&  2n+r(r+5)/2 + 2m +1.
\end{eqnarray}

This temporary array is de-allocated when the qpsolve primitive returns.

This formula may be simplified in the following cases :
\begin{itemize}
\item
if $n\gg m$, that is the number of constraints $m$ is
negligible with respect to the number of unknowns $n$, then the
memory required is $O(n)$,

\item
if $m\gg n$, that is the number of unknowns $n$ is
negligible with respect to the number of constraints $m$, then the
memory required is $O(m)$,

\item if $m=n$, then the memory required is $O(n^2)$.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Internal design}

The original Goldfarb/Idnani algorithm \cite{GoldfarbIdnani1982,GoldfarbIdnani1983} 
was considered as a building block for a Sequential Quadratic Programming solver.
The original package provides two routines~:
\begin{itemize}
\item solve.QP.f containing routine qpgen2 which implements the
algorithm for dense matrices,
\item solve.QP.compact.f containing routine qpgen1 which
implements the algorithm for sparse matrices.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Example for \scifun{qp\_solve}}

We search for $\bx \in \RR^6$ which 
minimize a linear-quadratic optimization problem with 3 
equality constraints and 2 inequality constraints.
The following script defines the input arguments of the 
\scifun{qld} function.

\lstset{language=scilabscript}
\begin{lstlisting}
// Find x in R^6 such that:
// x'*C1 = b1 (3 equality constraints i.e me=3)
C1= [ 1,-1, 2;
     -1, 0, 5;
      1,-3, 3;
      0,-4, 0;
      3, 5, 1;
      1, 6, 0];
b1=[1;2;3];
// x'*C2 >= b2 (2 inequality constraints)
C2= [ 0 ,1;
     -1, 0;
      0,-2;
     -1,-1;
     -2,-1;
      1, 0];
b2=[ 1;-2.5];
// and minimize 0.5*x'*Q*x - p'*x with
p=[-1;-2;-3;-4;-5;-6]; 
Q=eye(6,6);
me=3;
[x,iact,iter,f]=qp_solve(Q,p,[C1 C2],[b1;b2],me)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[x,iact,iter,f]=qp_solve(Q,p,[C1 C2],[b1;b2],me)
 f  =
  - 14.843248  
 iter  =
    5.  
    0.  
 iact  =
    1.  
    3.  
    2.  
    4.  
    0.  
 x  =
    1.7975426  
  - 0.3381487  
    0.1633880  
  - 4.9884023  
    0.6054943  
  - 3.1155623  
\end{lstlisting}

We see that only the linear constraints (1 to 4) are active, 
since the nonzero entries of the \scivar{iact} vector are 1, 2, 3 and 4.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The \scifun{qld} function}

In this section, we present the \scifun{qld} function, 
which allows to solve linear-quadratic optimization problems.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Mathematical point of view}
This kind of optimization is the minimization of function 
$f(x)$ with $$f(x) = \frac{1}{2} x^T Q x + p^T x$$ under:
\begin{itemize}
\item no constraints,
\item \textit{or} inequality constraints \Ref{qopt-contraint1},
\item \textit{or} inequality constraints and bound constraints 
(\Ref{qopt-contraint1} and \Ref{qopt-contraint2}),
\item \textit{or} inequality constraints, bound constraints and equality 
constraints (\Ref{qopt-contraint1}, \Ref{qopt-contraint2} and 
\Ref{qopt-contraint3}).
\end{itemize}

\begin{equation}
\label{qopt-contraint1}
C x \leq b
\end{equation}
\begin{equation}
\label{qopt-contraint2}
c_i \leq x \leq c_s
\end{equation}
\begin{equation}
\label{qopt-contraint3}
C_e x = b_e
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Scilab function}
The \scifun{qld} function are designed for linear optimization programming. 
As opposed to the \scifun{quapro} function of the Quapro toolbox, the \scifun{qld} 
function can only be used when the $Q$ matrix is positive definite.

The \scifun{qld} function is based on the Fortran 77 source code 
\verb|ql0001.f|.
This routine has been written by 
K. Schittkowski from the University of Bayreuth in Germany.
The implementation is a modification of routines
due to Prof. M.J.D. Powell at the University of Cambridge.
The algorithm is a modification of an algorithm proposed by 
M.J.D. Powell, from the university of Cambridge (1983).
A minor modification was performed by A.L. Tits 
and J.L. Zhou from the University of Maryland.
The Fortran source code indicates the version 1.4, March 1987.

The routine originally designed by Powell was named \verb|ZQPCVX| \cite{Powell1983}.

The algorithm is briefly described in the header of the Fortran 
source code.
The quadratic programming method proceeds from an initial Cholesky
decomposition of the objective function matrix, to calculate the
uniquely determined minimizer of the unconstrained problem.
Successively all violated constraints are added to a working set 
and a minimizer of the objective function subject to all constraints
in this working set is computed. It is possible that constraints
have to leave the working set.

Notice that the \scifun{fsqp} solver by Lawrence, Zhou and Tits uses 
\verb|ql0001.f| as the solver for the quadratic sub-problem.

The \scifun{qld} function cannot solve problems based on sparse matrices.

For more details about this function, please refer to 
\href{http://www.scilab.org/product/man/qld.html}{
\underline{Scilab online help for \textit{qld}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Example for \scifun{qld}}

We search for $\bx \in \RR^6$ which 
minimize a linear-quadratic optimization problem with 3 
equality constraints and 2 inequality constraints.
The following script defines the input arguments of the 
\scifun{qld} function.

\lstset{language=scilabscript}
\begin{lstlisting}
//Find x in R^6 such that:
//C1*x = b1 (3 equality constraints i.e me=3)
C1= [1,-1,1,0,3,1;
    -1,0,-3,-4,5,6;
     2,5,3,0,1,0];
b1=[1;2;3];
//C2*x <= b2 (2 inequality constraints)
C2=[0,1,0,1,2,-1;
    -1,0,2,1,1,0];
b2=[-1;2.5];
//with  x between ci and cs:
ci=[-1000;-10000;0;-1000;-1000;-1000];
cs=[10000;100;1.5;100;100;1000];
//and minimize 0.5*x'*Q*x + p'*x with
p=[1;2;3;4;5;6]; 
Q=eye(6,6);
//No initial point is given;
C=[C1;C2];
b=[b1;b2];
me=3;
[x,lagr]=qld(Q,p,C,b,ci,cs,me)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[x,lagr]=qld(Q,p,C,b,ci,cs,me)
 lagr  =
  - 1.5564027  
  - 0.1698164  
  - 0.7054782  
    0.3091368  
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
    0.         
 x  =
    1.7975426  
  - 0.3381487  
    0.1633880  
  - 4.9884023  
    0.6054943  
  - 3.1155623  
\end{lstlisting}

We see that only the linear constraints (1 to 4) are active, 
since \scivar{lagr(1:4)} are nonzero.
The constraint \#5 is not active, since \scivar{lagr(5)} is 
zero.
The components \#6 to \#17 represent the lower and upper 
bounds and are not active since their associated 
Lagrange multipliers are zero.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{A Guideline to choose a linear-quadratic solver}
\label{section-quideline-choosequad}

We have presented several linear-quadratic solvers and 
other solvers are presented in the chapter devoted to the 
Quapro toolbox.
It might be difficult for a user to finally choose between
these solvers.

In order to simplify this choice, the following is a list 
of differences between the solvers.

\begin{itemize}
\item sparsity
  \begin{itemize}
  \item \scifun{qld}, \scifun{linpro} and \scifun{quapro} manage only dense matrices.
  \item \scifun{qp\_solve} and \scifun{qpsolve} can manage dense $Q$ with dense $C$, 
  or dense $Q$ with sparse $C$.
  \end{itemize}
\item positivity of $Q$
  \begin{itemize}
  \item \scifun{qld}, \scifun{qpsolve} and \scifun{qp\_solve} manage real positive definite 
  symmetric matrix $Q$.
  \item The \scifun{quapro} function only requires real symmetric matrix $Q$. 
  \end{itemize}
\item bounds
  \begin{itemize}
  \item \scifun{qld}, \scifun{qpsolve}, \scifun{linpro} and \scifun{quapro} explicitely allows to manage bounds.
  \item \scifun{qp\_solve} only manages linear inequality constraints.
  \end{itemize}
\end{itemize}

From the point of view of performance, differences are to be expected, 
since all these functions use a different algorithm.
The following is a brief list of the functions along with the authors
of their numerical algorithms.
\begin{itemize}
\item \scifun{qld} : designed by M.J.D. Powell (1983) and modified by K. Schittkowski, with minor modifications by A. Tits and J.L. Zhou.
\item \scifun{qp\_solve}, \scifun{qpsolve} : the Goldfarb-Idnani algorithm and the Quadprog package developed by Berwin A. Turlach.
\item \scifun{linpro}, \scifun{quapro} : the algorithm developed by Eduardo Casas Renteria and Cecilia Pola Mendez.
\end{itemize}

We do not know of any comparative analysis of the performances of 
these linear-quadratic algorithms. 
Users who have experience on this topic are encouraged to contact us.

