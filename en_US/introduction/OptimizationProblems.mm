<map version="0.8.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1221744079191" ID="Freemind_Link_1559647739" MODIFIED="1221744086792" TEXT="Optimization">
<node CREATED="1221744173284" ID="_" MODIFIED="1221814089513" POSITION="right" STYLE="bubble" TEXT="Discrete &#xa;Parameters"/>
<node CREATED="1221744173284" ID="Freemind_Link_628130160" MODIFIED="1221814080483" POSITION="right" STYLE="bubble" TEXT="Continuous &#xa;Parameters">
<node CREATED="1221744173284" ID="Freemind_Link_1988858221" MODIFIED="1221814096105" STYLE="bubble" TEXT="Several &#xa;Objectives"/>
<node CREATED="1221744173284" ID="Freemind_Link_43197580" MODIFIED="1221814101034" STYLE="bubble" TEXT="One &#xa;Objective">
<node CREATED="1221744173284" ID="Freemind_Link_1774475305" MODIFIED="1221814174468" STYLE="bubble" TEXT="Non Smooth"/>
<node CREATED="1221744173284" ID="Freemind_Link_177695147" MODIFIED="1221814141750" STYLE="bubble" TEXT="Smooth">
<node CREATED="1221744173284" ID="Freemind_Link_1686557710" MODIFIED="1221814105696" STYLE="bubble" TEXT="Without &#xa;Constraints"/>
<node CREATED="1221744173284" ID="Freemind_Link_392985894" MODIFIED="1221814113511" STYLE="bubble" TEXT="With &#xa;Constraints">
<node CREATED="1221744173284" ID="Freemind_Link_1128763213" MODIFIED="1221814119815" STYLE="bubble" TEXT="Bounds &#xa;Constraints"/>
<node CREATED="1221744173284" ID="Freemind_Link_1786962455" MODIFIED="1221814123392" STYLE="bubble" TEXT="Linear &#xa;Constraints"/>
<node CREATED="1221744173284" ID="Freemind_Link_1234595172" MODIFIED="1221814127607" STYLE="bubble" TEXT="Non-linear &#xa;Constraints">
<node CREATED="1221744173284" ID="Freemind_Link_696939723" MODIFIED="1221814180924" STYLE="bubble" TEXT="Linear &#xa;Objective"/>
<node CREATED="1221744173284" ID="Freemind_Link_924643038" MODIFIED="1221814184923" STYLE="bubble" TEXT="Quadratic &#xa;Objective"/>
<node CREATED="1221744173284" ID="Freemind_Link_1268947282" MODIFIED="1221814190139" STYLE="bubble" TEXT="Non-linear &#xa;Objective">
<node CREATED="1221744173284" ID="Freemind_Link_1926085971" MODIFIED="1221814201963" STYLE="bubble" TEXT="1-10 &#xa;Unknowns"/>
<node CREATED="1221744173284" ID="Freemind_Link_448591618" MODIFIED="1221814208314" STYLE="bubble" TEXT="10-100 &#xa;Unknowns"/>
<node CREATED="1221744173284" ID="Freemind_Link_923486166" MODIFIED="1221814214834" STYLE="bubble" TEXT="&gt;100 &#xa;Unknowns"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
