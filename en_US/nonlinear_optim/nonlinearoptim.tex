% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

%% Chapter 1
\chapter{Non-linear optimization}

The goal of this chapter is to present the current features
of the \scifun{optim} primitive Scilab. The \scifun{optim} primitive allows to optimize
a problem with a nonlinear objective without constraints or with bound
constraints. 

In this chapter, we describe both the internal design of the \scifun{optim} primitive.
We analyse in detail the management of the cost function. 
The cost function and its gradient can be computed using
a Scilab function, a C function or a Fortran 77 function. 
The linear algebra components are analysed, since they are used 
at many places in the algorithms.
Since the management of memory is a crucial feature of
optimization solvers, the current behaviour of Scilab with respect to
memory is detailed here. 

Three non-linear solvers are connected to the \scifun{optim} 
primitive, namely, a BFGS Quasi-Newton solver, a L-BFGS solver, 
and a Non-Differentiable solver. In this chapter we analyse each solver and present 
the following features :
\begin{itemize}
\item the reference articles or reports,
\item the author,
\item the management of memory,
\item the linear algebra system, especially the algorithm name and
if dense/sparse cases are taken into account,
\item the line search method.
\end{itemize}

The \href{http://www.scilab.org/product/man/optim.html}{\underline{Scilab online help}}
is a good entry point for this function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Mathematical point of view}

The problem of the non linear optimization is to find the solution of 
$$
\min_x f(x)
$$
with bounds constraints or without constraints and with $f:\mathbb{R}^n \rightarrow \mathbb{R}$ the cost function.

\section{Optimization features}
Scilab offers three non-linear optimization methods:
\begin{itemize}
\item Quasi-Newton method with BFGS formula without constraints or with bound constraints,
\item Quasi-Newton with limited memory BFGS (L-BGFS) without constraints or with bound constraints,
\item Bundle method for non smooth functions (half derivable functions, non-differentiable problem) 
without constraints.
\end{itemize}

Problems involving non linear constraints cannot be solved with the current 
optimization methods implemented in Scilab.
Non smooth problems with bounds constraints cannot be solved with 
the methods currently implemented in Scilab.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Optimization routines}

Non-linear optimization in Scilab is based on a subset of the \textsc{Modulopt} 
library, developed at \textsc{Inria}. 
The library which is used by optim was created by the Modulopt project at INRIA and 
developped by Bonnans, Gilbert and Lemar�chal \cite{C-BonGilLemSag06}.

This section lists the routines used according to the optimization method used.

The following is the list of solvers currently available in
Scilab, and the corresponding fortran routine :
\begin{itemize}
\item "qn" without constraints : a Quasi-Newton method without constraints, n1qn1,
\item "qn" with bounds constraints : Quasi-Newton method with bounds constraints, qnbd,
\item "gc" without constraints : a Limited memory BGFS methoud without constraints, n1qn3,
\item "gc" with bounds constraints : a Limited memory BGFS with bounds constraints, gcbd,
\item "nd" without constraints : a Non smooth method without constraints, n1fc1.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The cost function}

The communication protocol used by optim is direct, that is, the 
cost function must be passed as a callback argument to the "optim" primitive.
The cost function must compute the objective and/or the gradient
of the objective, depending on the input integer flag "ind".

In the most simple use-case, the cost function is a Scilab function, with 
the following header~: 
\lstset{language=scilabscript}
\begin{lstlisting}
[f,g,ind]=costf(x,ind)
\end{lstlisting}
where "x" is the current value 
of the unknown and "ind" is the integer flag which states if "f", "g" or both
are to be computed.

The cost function is passed to the optimization solver as a
callback, which is managed with the fortran 77 callback system. In that
case, the name of the routine to call back is declared as "external" in
the source code.
The cost function may be provided in the following ways :
\begin{itemize}
\item the cost function is provided as a Scilab script,
\item the cost function is provided as a C or fortran 77 compiled
routine.
\end{itemize}

If the cost function is a C or fortran 77 source code, the cost function 
can be statically or dynamically linked against Scilab.
Indeed, Scilab dynamic link features, such as ilib\_for\_link for example, can be used
to provide a compiled cost function.

In the following paragraph, we analyse the very internal
aspects of the management of the cost function.

This switch is managed at the gateway level, in the sci\_f\_optim
routine, with a "if" statement~:
\begin{itemize}
\item if the cost function is compiled, the
"foptim" symbol is passed,
\item if not, the "boptim" symbol is passed.
\end{itemize}
In the case where the cost function is a Scilab script, the
"boptim" routine performs the copy of the input local arguments into
Scilab internal memory, calls the script, and finally copy back the
output argument from Scilab internal memory into local output variables.
In the case where the cost function is compiled, the computation
is based on function pointers, which are managed at the C level in
optimtable.c. The optimization function is configured by the "setfoptim"
C service, which takes as argument the name of the routine to callback.
The services implemented in AddFunctionInTable.c are used,
especially the function "AddFunctionInTable", which takes the
name of the function as input argument and searches the corresponding
function address, be it in statically compiled libraries or in
dynamically compiled libraries. This allows the optimization solvers to
callback dynamically linked libraries. These names and addresses are
stored in the hashmap FTab\_foptim, which maps function names to function
pointer. The static field foptimfonc with type foptimf is then set to
the address of the function to be called back. When the optimization
solver needs to compute the cost function, it calls the "foptim" C void
function which in returns calls the compiled cost function associated to
the configured address (*foptimfonc).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Linear algebra}

The linear algebra which is used in the "optim" primitive is dense. 
Generally, the linear algebra is inlined and there is no use 
the BLAS API. This applies to all optimization methods, except "gcbd". 
This limits the performance of the optimization, because optimized libraries like ATLAS 
cannot not used. There is only one exception : the L-BFGS  
with bounds constraints routine gcbd uses the "dcopy" routine of the 
BLAS API. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Management of memory}
The optimization solvers requires memory, especially to store the value of the cost 
function, the gradient, the descent direction, but most importantly, 
the approximated \emph{Hessian of the cost function}.

Most of the memory is required by the approximation of the Hessian matrix.
If the full approximated Hessian is stored, as in the BFGS quasi-Newton method, the amount of 
memory is the square of the dimension of the problem $O(n^2)$, where $n$ is the size of the unknown.
When a quasi-Newton method with limited memory is used, only a given 
number $m$ of vectors of size $n$ are stored. 

This memory is allocated by Scilab, inside the stack and the
storage area is passed to the solvers as an input argument. This large
storage memory is then split into pieces like a piece of cake by each
solver to store the data. The memory system used by the fortran solvers
is the fortran 77 "assumed-size-dummy-arrays" mechanism, based on "real
arrayname(*)" statements.

The management of memory is very important for large-scale
problems, where $n$ is from 100 to 1000.
One main feature of one of the L-BFGS algorithms is to limit the memory required.
More precisely, the following is a map from the algorithm to the memory required, 
as the number of required double precision values.
\begin{itemize}
\item Quasi-Newton BFGS "qn" without constraints (n1qn1) : $n(n+13)/2$,
\item Quasi-Newton BFGS "qn" with bounds constraints (qnbd) : $n(n+1)/2 + 4n + 1 $,
\item Limited Memory BFGS "gc" without constraints (n1qn3) : $4n + m(2n + 1) $,
\item Limited Memory BFGS "gc" with bounds constraints (gcbd) : $n(5 + 3nt) + 2nt +1$ with $nt= max(1,m/3)$,
\item Non smooth method without constraints (n1fc1) : $(n+4)m/2 +  (m+9)*m+8+5n/2 $.
\end{itemize}

Note that n1fc1 requires an additionnal $2(m+1)$ array of integers.
Simplifying these array sizes leads to the following map, which
clearly shows why Limited Memory BFGS algorithms in Scilab are more
suitable for large problems. This explains why the name "cg" was chosen:
it refers to the Conjugate Gradient method, which stores only one 
vector in memory. But the name "cg" is wrongly chosen and this is why we consistently
use L-BFGS to identify this algorithm.
\begin{itemize}
\item Quasi-Newton "qn" without constraints (n1qn1) : $O(n^2)$,
\item Quasi-Newton "qn" with bounds constraints (qnbd) : $O(n^2)$,
\item Limited Memory BFGS "gc" without constraints (n1qn3) : $O(n)$,
\item Limited Memory BFGS "gc" with bounds constraints (gcbd) : $O(n)$,
\item Non smooth method without constraints (n1fc1) : $O(n)$.
\end{itemize}

That explains why L-BFGS methods associated with the "gc" option
of the optim primitive are recommended for large-scale optimization.
It is known that L-BFGS convergence may be slow for 
large-scale problems (see \cite{numericaloptimization}, chap. 9).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Quasi-Newton "qn" without constraints : n1qn1}
The author is C. Lemarechal, 1987. There is no reference report
for this solver.

The following is the header for the n1qn1 routine :

\begin{verbatim}
      subroutine n1qn1 (simul,n,x,f,g,var,eps,
     1     mode,niter,nsim,imp,lp,zm,izs,rzs,dzs)
c!but
c     minimisation d une fonction reguliere sans contraintes
c!origine
c     c. lemarechal, inria, 1987
c     Copyright INRIA
c!methode
c     direction de descente calculee par une methode de quasi-newton
c     recherche lineaire de type wolfe
\end{verbatim}

The following is a description of the arguments of this routine.
\begin{itemize}
\item simul    : point d'entree au module de simulation (cf normes modulopt i).
n1qn1 appelle toujours simul avec indic = 4 ; le module de
simulation doit se presenter sous la forme subroutine simul(n,x, f, g, izs, rzs, dzs)
et �tre declare en external dans le programme appelant n1qn1.
\item n (e)    : nombre de variables dont depend f.
\item x (e-s)   : vecteur de dimension n ; en entree le point initial ;
en sortie : le point final calcule par n1qn1.
\item f (e-s)   : scalaire ; en entree valeur de f en x (initial), en sortie
valeur de f en x (final).
\item g (e-s)   : vecteur de dimension n : en entree valeur du gradient en x
(initial), en sortie valeur du gradient en x (final).
\item var (e)   : vecteur strictement positif de dimension n. amplitude de la
modif souhaitee a la premiere iteration sur x(i).une bonne
valeur est 10\% de la difference (en valeur absolue) avec la
coordonee x(i) optimale
\item eps (e-s) : en entree scalaire definit la precision du test d'arret.
Le programme considere que la convergence est obtenue lorque il lui
est impossible de diminuer f en attribuant � au moins une coordonn�e
x(i) une variation superieure a eps*var(i).
En sortie, eps contient le carr� de la norme du gradient en x (final).
\item mode (e)     : definit l'approximation initiale du hessien
  \begin{itemize}
  \item =1 n1qn1 l'initialise lui-meme
  \item  =2 le hessien est fourni dans zm sous forme compressee (zm
  contient les colonnes de la partie inferieure du hessien)
  \end{itemize}
\item niter (e-s)  : en entree nombre maximal d'iterations : en sortie nombre 
d'iterations reellement effectuees.
\item nsim (e-s)  : en entree nombre maximal d'appels a simul (c'est a dire 
avec indic = 4). en sortie le nombre de tels appels reellement faits.
\item imp (e)   : contr�le les messages d'impression :
  \begin{itemize}
  \item = 0 rien n'est imprime
  \item = 1 impressions initiales et finales
  \item = 2 une impression par iteration (nombre d'iterations, nombre d'appels a simul, valeur courante de f).
  \item >=3 informations supplementaires sur les recherches lineaires ;
  tres utile pour detecter les erreurs dans le gradient.
  \end{itemize}
\item lp (e)    : le numero du canal de sortie, i.e. les impressions
 commandees par imp sont faites par write (lp, format).
\item zm     : memoire de travail pour n1qn1 de   dimension n*(n+13)/2.
\item izs,rzs,dzs memoires reservees au simulateur (cf doc)
\end{itemize}

The n1qn1 solver is an interface to the n1qn1a routine, which
really implements the optimization method. The n1qn1a file counts
approximately 300 lines. The n1qn1a routine is based on the following
routines~:
\begin{itemize}
\item simul : computes the cost function,
\item majour : probably (there is no comment) an update of the
BFGS matrix.
\end{itemize}
Many algorithms are in-lined, especially the line search and the
linear algebra.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Management of the approximated Hessian matrix}

The current BFGS implementation is based on a approximation of the Hessian \cite{numericaloptimization},
which is based on Cholesky decomposition, i.e. the approximated 
Hessian matrix is decomposed as $G=LDL^T$, where $D$ is a diagonal $n\times n$ 
matrix and $L$ is a lower triangular $n\times n$ matrix with unit diagonal.
To compute the descent direction, the linear system $Gd=LDL^Td = -g$ is solved.

The memory requirements for this method is $O(n^2)$ because the 
approximated Hessian matrix computed from the BFGS formula is stored 
in compressed form so that only the lower part of the approximated 
Hessian matrix is stored. This is why this method is not recommended for large-scale 
problems (see \cite{numericaloptimization}, chap.9, introduction).

The approximated hessian $H\in\RR^{n\times n}$ is stored as the 
vector $h\in\RR^{n_h}$ which has size $n_h = n(n+1)/2$.
The matrix is stored in factored form as following 
\begin{eqnarray}
h = \left(D_{11} L_{21} \ldots L_{n1} | H_{21} D_{22} \ldots L_{n2} | 
\ldots | D_{n-1 n-1} L_{n n-1} | D_{nn} \right).
\end{eqnarray}
Instead of a direct acces to the factors of $D$ and $L$, 
integers algebra is necessary to access to the data stored in the vector $h$. 

The algorithm presented in figure \ref{algo-diagonal-compressedsymetric} 
is used to set the diagonal terms the diagonal terms of $D$, the 
diagonal matrix of the Cholesky decomposition of the approximated Hessian.
The right-hand side $\frac{0.01 c_{max}}{v_i^2}$ of this initialization is 
analysed in the next section of this document.

\begin{figure}[htbp]
\begin{algorithmic}
\STATE $k\gets 1$
\FOR{$i$ = 1 to $n$} 
\STATE $h(k) = \frac{0.01 c_{max}}{v_i^2}$
\STATE $k \gets k + n + 1 - i$
\ENDFOR
\end{algorithmic}
\caption{Loop over the diagonal terms of the Cholesky decomposition of the 
approximated Hessian}
\label{algo-diagonal-compressedsymetric}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Solving the linear system of equations}

The linear system of equations $Gd=LDL^Td = -g$ must be solved to 
computed the descent direction $d\in\RR^n$.
This direction is computed by the following algorithm
\begin{itemize}
\item compute $w$ so that $Lw = -g$,
\item computed $d$ so that $DL^T d = w$.
\end{itemize}
This algorithm requires $O(n^2)$ operations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Line search}

The line search is based on the algorithms developped 
by Lemar�chal \cite{Lemarechal1981}. 
It uses a cubic interpolation.

The Armijo condition for sufficient decrease is used in the 
following form 
\begin{eqnarray}
\label{n1qn1-armijo}
f(x_k+\alpha p_k) - f(x_k) \leq c_1 \alpha \nabla f_k^T p_k
\end{eqnarray}
with $c_1 = 0.1$. The following fortran source code illustrates
this condition
\begin{verbatim}
      if (fb-fa.le.0.10d+0*c*dga) go to 280
\end{verbatim}
where $fb = f(x_k+\alpha p_k)$, $fa = f(x_k)$, 
$c = \alpha$ and $dga = \nabla f_k^T p_k$ is the local directional 
derivative.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Initial Hessian matrix}

Several modes are available to compute the initial Hessian matrix, 
depending on the value of the \emph{mode} variable
\begin{itemize}
\item if mode = 1, n1qn1 initializes the matrix by itself,
\item if mode = 2, the hessian is provided in compressed form, where
only the lower part of the symetric hessian matrix is stored.
\end{itemize}
An additionnal mode=3 is provided but the feature is not clearly documented.
In Scilab, the n1qn1 routine is called with mode = 1 by default.
In the case where a hot-restart is performed, the mode = 3 is enabled.

If mode = 1 is chosen, the initial Hessian matrix $H^0$ is computed 
by scaling the identity matrix 

\begin{eqnarray}
H^0 = I \delta
\end{eqnarray}
where $\delta\in\RR^n$ is a $n$-vector and $I$ is the $n\times n$ identity matrix.
The scaling vector $\delta\in\RR^n$ is based on the gradient at the initial guess
$g^0 = g(x_0) = \nabla f(x_0) \in \RR^n$ and a scaling vector $v\in\RR^n$, given by the user
\begin{eqnarray}
\delta_i = \frac{0.01 c_{max}}{v_i^2}
\end{eqnarray}
where $c_{max}>0$ is computed by 
\begin{eqnarray}
c_{max} = \max\left(1.0 , \max_{i=1,n}(|g_i^0)|) \right)
\end{eqnarray}

In the Scilab interface for \emph{optim}, the scaling vector is 
set to $0.1$ :

\begin{eqnarray}
v_i = 0.1, \qquad i=1,n.
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Termination criteria}

The following list of parameters are taken into account by the solver 
\begin{itemize}
\item \scivar{niter}, the maximum number of iterations (default value is 100),
\item \scivar{nap}, the maximum number of function evaluations (default value is 100),
\item \scivar{epsg}, the minimum length of the search direction (default value is $\%eps\approx 2.22e-16$).
\end{itemize}

The other parameters \scivar{epsf} and \scivar{epsx} are not used.
The termination condition is not based on the gradient, as the name \scivar{epsg} would indicate.

The following is a list of termination conditions which are taken 
into account in the source code.
\begin{itemize}
\item The iteration is greater than the maximum.
\begin{verbatim}
      if (itr.gt.niter) go to 250
\end{verbatim}
\item The number of function evaluations is greater than the maximum.
\begin{verbatim}
      if (nfun.ge.nsim) go to 250
\end{verbatim}
\item The directionnal derivative is positive, so that the direction $d$ is 
not a descent direction for $f$.
\begin{verbatim}
      if (dga.ge.0.0d+0) go to 240
\end{verbatim}
\item The cost function set the indic flag (the \scivar{ind} parameter) 
to 0, indicating that the optimization must terminate.
\begin{verbatim}
      call simul (indic,n,xb,fb,gb,izs,rzs,dzs)
      [...]
      go to 250
\end{verbatim}
\item The cost function set the indic flag to a negative value
indicating that the function cannot be evaluated for the given $x$.
The step is reduced by a factor 10, but gets below a limit so that
the algorithm terminates.
\begin{verbatim}
      call simul (indic,n,xb,fb,gb,izs,rzs,dzs)
      [...]
      step=step/10.0d+0
      [...]
      if (stepbd.gt.steplb) goto 170
      [...]
      go to 250
\end{verbatim}
\item The Armijo condition is not satisfied and step size is below a limit during the line search.
\begin{verbatim}
      if (fb-fa.le.0.10d+0*c*dga) go to 280
      [...]
      if (step.gt.steplb) go to 270
\end{verbatim}
\item During the line search, a cubic interpolation is computed and the computed
minimum is associated with a zero step length.
\begin{verbatim}
      if(c.eq.0.0d+0) goto 250
\end{verbatim}
\item During the line search, the step length is lesser than a computed 
limit.
\begin{verbatim}
      if (stmin+step.le.steplb) go to 240
\end{verbatim}
\item The rank of the approximated Hessian matrix is lesser than 
$n$ after the update of the Cholesky factors.
\begin{verbatim}
      if (ir.lt.n) go to 250
\end{verbatim}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{An example}

The following script illustrates that the gradient may be very slow, 
but the algorithm continues. This shows that the termination 
criteria is not based on the gradient, but on the length of the step.
The problem has two parameters so that $n=2$.
The cost function is the following 
\begin{eqnarray}
f(\bx)=x_1^p + x_2^p
\end{eqnarray}
where $p\geq 0$ is an even integer.
Here we choose $p=10$.
The gradient of the function is 
\begin{eqnarray}
g(\bx) = \nabla f(\bx)=(p x_1^{p-1} , p x_2^{p-1})^T
\end{eqnarray}
and the Hessian matrix is 
\begin{eqnarray}
H(\bx) = 
\left(
\begin{array}{cc}
p (p-1)x_1^{p-2} & 0\\
0 & p (p-1)x_2^{p-2}\\
\end{array}
\right)
\end{eqnarray}

The optimum of this optimization problem is at 
\begin{eqnarray}
\bx^\star=(0,0)^T.
\end{eqnarray}

The following Scilab script defines the cost function,
checks that the derivatives are correctly computed and 
performs an optimization.
At each iteration, the norm of the gradient of the cost function
is displayed so that one can see if the algorithm
terminates when the gradient is small.

\lstset{language=scilabscript}
\begin{lstlisting}
function [ f , g , ind ] = myquadratic ( x , ind )
  p = 10
  if ind == 1 | ind == 2 | ind == 4 then
    f = x(1)^p + x(2)^p;
  end
  if ind == 1 | ind == 2 | ind == 4 then
    g(1) = p * x(1)^(p-1)
    g(2) = p * x(2)^(p-1)
  end
  if ind == 1 then
    mprintf("|x|=%e, f=%e, |g|=%e\n",norm(x),f,norm(g))
  end
endfunction
function f = quadfornumdiff ( x )
  f = myquadratic ( x , 2 )
endfunction
x0 = [-1.2 1.0];
[ f , g ] = myquadratic ( x0 , 4 );
mprintf ( "Computed f(x0) = %f\n", f);
mprintf ( "Computed g(x0) = \n");disp(g');
mprintf ( "Expected g(x0) = \n");disp(derivative(quadfornumdiff,x0'))
nap = 100
iter = 100
epsg = %eps
[ fopt , xopt , gradopt ] = optim ( myquadratic , x0 , ...
  "ar" , nap , iter , epsg , imp = -1)
\end{lstlisting}

The script produces the following output.

\lstset{language=scilabscript}
\begin{lstlisting}
-->[ fopt , xopt , gradopt ] = optim ( myquadratic , x0 , ...
   "ar" , nap , iter , epsg , imp = -1)
|x|=1.562050e+000, f=7.191736e+000, |g|=5.255790e+001
|x|=1.473640e+000, f=3.415994e+000, |g|=2.502599e+001
|x|=1.098367e+000, f=2.458198e+000, |g|=2.246752e+001
|x|=1.013227e+000, f=1.092124e+000, |g|=1.082542e+001
|x|=9.340864e-001, f=4.817592e-001, |g|=5.182592e+000
[...]
|x|=1.280564e-002, f=7.432396e-021, |g|=5.817126e-018
|x|=1.179966e-002, f=3.279551e-021, |g|=2.785663e-018
|x|=1.087526e-002, f=1.450507e-021, |g|=1.336802e-018
|x|=1.002237e-002, f=6.409611e-022, |g|=6.409898e-019
|x|=9.236694e-003, f=2.833319e-022, |g|=3.074485e-019
Norm of projected gradient lower than   0.3074485D-18.
 gradopt  =
   1.0D-18 *
    0.2332982    0.2002412  
 xopt  =
    0.0065865    0.0064757  
 fopt  =
    2.833D-22  
\end{lstlisting}

One can see that the algorithm terminates when the gradient 
is extremely small $g(\bx)\approx 10^{-18}$.
The cost function is very near zero $f(\bx)\approx 10^{-22}$,
but the solution is not accurate only up to the 3d digit.

This is a very difficult test case for optimization 
solvers. The difficulty is because the function is extremely
\emph{flat} near the optimum. If the termination
criteria was based on the gradient, the algorithm would stop
in the early iterations. Because this is not the case,
the algorithm performs significant iterations which are 
associated with relatively large steps.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Quasi-Newton "qn" with bounds constraints : qnbd}
The comments state that the reference report is an INRIA report by
F. Bonnans \cite{bonnans1983}.
The solver qnbd is an interface to the zqnbd routine. The zqnbd
routine is based on the following routines :
\begin{itemize}
\item calmaj : calls majour, which updates the BFGS matrix,
\item proj : projects the current iterate into the bounds
constraints,
\item ajour : probably (there is no comment) an update of the BFGS
matrix,
\item rlbd : line search method with bound constraints,
\item simul : computes the cost function
\end{itemize}
The rlbd routine is documented as using an extrapolation method to
computed a range for the optimal t parameter. The range is then reduced
depending on the situation by :
\begin{itemize}
\item a dichotomy method,
\item a linear interpolation,
\item a cubic interpolation.
\end{itemize}
The stopping criteria is commented as "an extension of the Wolfe
criteria".
The linear algebra does not use the BLAS API. It is in-lined, so
that connecting the BLAS may be difficult.
The memory requirements for this method are $O(n^2)$, which shows
why this method is not recommended for large-scale problems (see \cite{numericaloptimization},
chap.9, introduction).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{L-BFGS "gc" without constraints : n1qn3}
The comments in this solver are clearly written.
The authors are Jean Charles Gilbert, Claude Lemarechal, 1988. The
BFGS update is based on the article \cite{Nocedal1980LimitedBFGS}.
The solver n1qn3 is an interface to the n1qn3a routine. The
architecture is clear and the source code is well commented. The n1qn3a
routine is based on the following routines :
\begin{itemize}
\item prosca : performs a vector x vector scalar product,
\item simul : computes the cost function,
\item nlis0 : line search based on Wolfe criteria, extrapolation
and cubic interpolation,
\item ctonb : copies array u into v,
\item ddd2 : computed the descent direction by performing the
product hxg.
\end{itemize}
The linear algebra is dense, which limits the feature to small
size optimization problems. The linear algebra does not use the BLAS API
but is based on the prosca and ctonb routines. The prosca routine is a
call back input argument of the n1qn3 routine, connected to the fuclid
routine. This implements the scalar product, but without optimizaion.
Connecting BLAS may be easy for n1qn3.
The algorithm is a limited memory BFGS method with m levels, so
that the memory cost is $O(n)$. It is well suited for medium-scale
problems, although convergence may be slow (see \cite{numericaloptimization}, chap. 9, p.227).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{L-BFGS "gc" with bounds constraints : gcbd}
The author is F. Bonnans, 1985. There is no reference report for
gcbd.
The gcbd solver is an interface to the zgcbd routine, which really
implements the optimization method. The zgcbd routine is based on the
following routines :
\begin{itemize}
\item simul : computes the cost function
\item proj : projects the current iterate into the bounds
constraints,
\item majysa : updates the vectors $y=g(k+1)-g(k)$, $s=x(k+1)-x(k)$,
$ys$,
\item bfgsd : updates the diagonal by Powell diagonal BFGS,
\item shanph : scalse the diagonal by Shanno-Phua method,
\item majz : updates $z$,$zs$,
\item relvar : computes the variables to relax by Bertsekas
method,
\item gcp : gradient conjugate method for $Ax=b$,
\item dcopy : performs a vector copy (BLAS API),
\item rlbd : line search method with bound constraints.
\end{itemize}
The linear algebra is dense. But zgcbd uses the "dcopy" BLAS routine,
which allows for some optimizations.
The algorithm is a limited memory BFGS method with $m$ levels, so
that the memory cost is $O(n)$. It is well suited for medium-scale
problems, although the convergence may be slow (see \cite{numericaloptimization}, 
chap. 9, p.227).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Non smooth method without constraints : n1fc1}

This routine is probably due to Lemar�chal, who is an expert of this topic. 
References for this algorithm include the "Part II, Nonsmooth optimization" 
in \cite{C-BonGilLemSag06}, and the in-depth presentation in 
\cite{Hiriart-Urruty-partI-1993,Hiriart-Urruty-partII-1993}.

The n1fc1 solver is an interface to the n1fc1a routine, which
really implements the optimization method. The n1fc1a routine is based
on the following routines :
\begin{itemize}
\item simul : computes the cost function,
\item fprf2 : computes the direction,
\item frdf1 : reduction du faisceau
\item nlis2 : line search,
\item prosca : performs a vector x vector scalar product.
\end{itemize}
It is designed for functions which have a non-continuous derivative (e.g. the objective 
function is the maximum of several continously differentiable functions).


