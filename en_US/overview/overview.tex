% Copyright (C) 20010 - 2011 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\chapter{Overview of optimization features in Scilab}

Scilab provides a high-level matrix language and allows to 
define complex mathematical models and to easily connect to 
existing libraries. 
This is why optimization is an important practical topic in 
Scilab, which provides tools to solve linear and nonlinear 
optimization problems by a large collection of tools.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Overview}

In this section, we present industrial-grade solvers available in Scilab. 

The figure \ref{fig-scilab-features} presents an overview of the type of optimization problems which can be solved by Scilab.
\begin{itemize}
 \item For the  constraint columns, the letter "l" means linear, the letter "n" means non-linear and "l*" means linear constraints in spectral sense. 
 \item For the gradient column, the letter "e" means that the gradient can be estimated by the solver. 
 \item For the problem size column, the letters "s", "m" and "l" respectively mean small, medium and large. 
\end{itemize}

\begin{figure}
\begin{center}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\textbf{Objective} & \textbf{Bounds} & \textbf{Equality} & \textbf{Inequa-} & \textbf{Size} & \textbf{Gradient} & \textbf{Solver} \\
                   &                 &                   & \textbf{lities}  &               & \textbf{Needed}   &                 \\
\hline
Linear    & y      & l        & l            & m            & -               & linpro      \\
\hline
Quadratic & y      & l        & l             & m            & -               & quapro      \\
Quadratic & y      & l        & l            & l            & -               & qpsolve     \\
Quadratic & y      & l        & l            & m            & -               & qld         \\
\hline
Non-Linear& y      &          &              & l            & y               & optim       \\
Non-Linear&        &          &              & s            & n               & fminsearch  \\
Non-Linear& y      &          &              & s            & n               & neldermead  \\
Non-Linear& y      &          &              & s            & n               & optim\_ga    \\
Non-Linear&        &          &              & s            & n               & optim\_sa    \\
\hline
N.L.S.    &        &          &              & l            & optional        & lsqrsolve   \\
N.L.S.    &        &          &              & l            & optional        & leastsq     \\
\hline
Min-Max   & y      &          &              & m            & y               & optim/nd    \\
\hline
Multi-Obj.& y      &          &              & s            & n               & optim\_moga  \\
Multi-Obj.& y      &          & l*           & l            & n               & semidef     \\
\hline
Semi-Def. &        & l*       & l*           & l            & n               & lmisolve    \\
\hline
\end{tabular}
\end{center}
\caption{Scilab functions for numerical optimization. N.L.S. stands for Nonlinear Least Squares.}
\label{fig-scilab-features}
\end{figure}

\section{Focus on nonlinear optimization}

The ''optim'' function solves optimization problems with non-linear objectives, 
 with or without bound constraints on the unknowns. 
 The quasi-Newton method ''optim''/"qn" uses a Broyden-Fletcher-Goldfarb-Shanno 
 formula to update the approximate Hessian matrix. 
 The quasi-Newton method has a $O(n^2)$ memory requirement. 
 The limited memory BFGS algorithm ''optim''/"gc" is efficient for 
 large size problems due to its memory requirement in $O(n)$. 
 Finally, a bundle method is used to solve unconstrained, non-differentiable 
 problems (n1fc1). 
 For all these solvers, a function that computes the gradient g must 
 be provided. 
 That gradient can be computed using finite differences based on an optimal 
 step with the ''derivative'' function, for example.
 
The ''fminsearch'' function is based on the simplex algorithm of 
 Nelder and Mead (not to be confused with Dantzig�s simplex for linear optimization). 
 This unconstrained algorithm does not require the gradient of the cost function. 
 It is efficient for small problems, i.e. up to 10 parameters and its memory requirement 
 is only $O(n)$. 
 This algorithm is known to be able to manage "noisy" functions, i.e. situations 
 where the cost function is the sum of a general nonlinear function and a low magnitude 
 noise function. 
 The neldermead component provides three simplex-based algorithms which allow to 
 solve unconstrained and nonlinearly constrained optimization problems. 
 It provides an object oriented access to the options. 
 The fminsearch function is, in fact, a specialized use of the neldermead component.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Nonlinear optimization}

The flagship of Scilab is certainly the ''optim'' function, which provides a set of 5 algorithms 
for nonlinear unconstrained (and bound constrained) optimization problems.

\begin{figure}
\includegraphics[width=15cm]{overview/demo_rosenbrockplot}
\caption{Optimization of the Rosenbrock function by the optim function.}
\label{gif-optimrosenbrock}
\end{figure}

The ''optim'' function is an unconstrained (or bound constrained) nonlinear optimization solver.
The calling sequence is:
\lstset{language=scilabscript}
\begin{lstlisting}
fopt = optim(costf,x0)
fopt = optim(costf,"b",lb,ub,x0)
fopt = optim(costf,"b",lb,ub,x0,algo)
[fopt,xopt] = optim(...)
[fopt,xopt,gopt] = optim(...)
\end{lstlisting}
where
\begin{itemize}
 \item f is the objective function
 \item x0 is the initial guess
 \item lb is the lower bound
 \item ub is the upper bound
 \item algo is the algorithm
 \item fopt is the minimum function value
 \item xopt is the optimal point
 \item gopt is the optimal gradient
\end{itemize}

The ''optim'' function allows to use 3 different algorithms:
\begin{itemize}
 \item algo = "qn" : Quasi-Newton (the default solver) based on BFGS formula
 \item algo = "gc" : Limited Memory BFGS algorithm for large-scale optimization
 \item algo = "nd" : bundle method for non-differentiable problems (e.g. min-max)
\end{itemize}

The main features of the optim function are the following.
\begin{itemize}
 \item Provides efficient optimization solvers based on robust algorithms.
 \item Objective function f in Scilab macros or external (dynamic link).
 \item Extra parameters can be passed to the objective function (with a list or array).
 \item Robust implementation: 
 \begin{itemize}
   \item Quasi-Newton based on the update of the Cholesky factors.
   \item Line-Search of optim/qn based on a safeguarded cubic interpolation designed by Lemar�chal.
 \end{itemize}
\end{itemize}

In the following script, we compute the unconstrained optimum of the Rosenbrock function.
\lstset{language=scilabscript}
\begin{lstlisting}
function [f, g, ind]=rosenbrock(x, ind)
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
x0 = [-1.2 1.0];
[ fopt , xopt ] = optim ( rosenbrock , x0 )
\end{lstlisting}
The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[ fopt , xopt ] = optim ( rosenbrock , x0 )
 xopt  =
    1.    1.  
 fopt  =
    0.  
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Computing the derivatives by finite differences}

Scilab provides the derivative function which computes approximate gradients based on finite differences.
The calling sequence is:
\lstset{language=scilabscript}
\begin{lstlisting}
g = derivative(f,x)
g = derivative(f,x,h)
g = derivative(f,x,h,order)
[g,H] = derivative(...)
\end{lstlisting}
where
\begin{itemize}
 \item f is the objective function
 \item x is the point where to evaluate the gradient
 \item h is the step
 \item order is the order of the finite difference formula
 \item g is the gradient (or Jacobian matrix)
 \item H is the Hessian matrix
\end{itemize}

The main features of the optim function are the following.
\begin{itemize}
 \item Uses optimal step (manages limited precision of floating point numbers).
 \item Can handle any type of objective function: macro or external program or library.
 \item Provides order 1, 2 or 4 formulas.
\end{itemize}

In the following script, we compute the optimum of the Rosenbrock problem with finite differences.

\lstset{language=scilabscript}
\begin{lstlisting}
function f=rosenbrock(x)
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction
function [f, g, ind]=rosenbrockCost2(x, ind)
  if ((ind == 1) | (ind == 4)) then
    f = rosenbrock ( x );
  end
  if ((ind == 1) | (ind == 4)) then
    g= derivative ( rosenbrock , x.' , order = 4 );
  end
endfunction
x0 = [-1.2 1.0];
[ fopt , xopt ] = optim ( rosenbrockCost2 , x0 )
\end{lstlisting}

The figure \ref{gif-patternderivative} presents the pattern of the order 1, 2 and 4 
finite differences formulas in the ''derivative'' function.

\begin{figure}
\includegraphics[width=5cm]{overview/jacobian_patternO1}
\includegraphics[width=5cm]{overview/jacobian_patternO2}
\includegraphics[width=5cm]{overview/jacobian_patternO4}
\caption{Pattern of the order 1, 2 and 4 finite differences formulas in the ''derivative'' function.}
\label{gif-patternderivative}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Linear and Quadratic Optimization}

The quapro module is available through ATOMS:

\url{http://atoms.scilab.org/toolboxes/quapro}

The quapro module defines linear quadratic programming solvers. The matrices defining the cost and constraints must be full, but the quadratic term matrix is not required to be full rank.

The main features of the optim function are the following.
\begin{itemize}
 \item linpro : linear programming solver
 \item quapro : linear quadratic programming solver
 \item mps2linpro : convert lp problem given in MPS format to linpro format
\end{itemize}

To install the quapro module:
\lstset{language=scilabscript}
\begin{lstlisting}
atomsInstall("quapro");
\end{lstlisting}
and then re-start Scilab.

The linpro function can solve linear programs in general form:
\lstset{language=scilabscript}
\begin{lstlisting}
Minimize c'*x
A*x   <= b
Aeq*x  = beq
lb <= x <= ub
\end{lstlisting}

The following example is extracted from "Operations Research: applications and algorithms", 
Wayne L. Winstons, Section 5.2, "The Computer and Sensitivity Analysis", in the "Degeneracy and Sensitivity Analysis" subsection.
We consider the problem:
\lstset{language=scilabscript}
\begin{lstlisting}
Min -6*x1 - 4*x2 - 3*x3 - 2*x4  
such as:
2*x1 + 3*x2 +   x3 + 2*  x4 <= 400
  x1 +   x2 + 2*x3 +     x4 <= 150
2*x1 +   x2 +   x3 + 0.5*x4 <= 200
3*x1 +   x2 +            x4 <= 250;
x >= 0;
\end{lstlisting}

The following script allows to solve the problem.
\lstset{language=scilabscript}
\begin{lstlisting}
c = [-6 -4 -3 -2]';
A = [
     2 3 1 2
     1 1 2 1
     2 1 1 0.5
     3 1 0 1
     ];
b = [400 150 200 250]';
ci=[0 0 0 0]';
cs=[%inf %inf %inf %inf]';
[xopt,lagr,fopt]=linpro(c,A,b,ci,cs)
\end{lstlisting}

This produces : 
\lstset{language=scilabscript}
\begin{lstlisting}
xopt = [50,100,2.842D-14,0]
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Non-Linear Least Squares}

Scilab provides 2 solvers for nonlinear least squares:
\begin{itemize}
 \item lsqrsolve : Solves non-linear least squares problems, Levenberg-Marquardt algorithm.
 \item leastsq : Solves non-linear least squares problems (built over optim).
\end{itemize}

The main features of the optim function are the following.
\begin{itemize}
 \item Can handle any type of objective function: macro or external program or library.
 \item The gradient is optional.
\end{itemize}

In the following example, we are searching for the parameters of a system of ordinary differential 
equations which best fit experimental data. 
The context is a chemical reaction for processing waters with phenolic compounds. 
We use the leastsq functions in a practical case.

\lstset{language=scilabscript}
\begin{lstlisting}
function dy = myModel ( t , y , a , b ) 
   // The right-hand side of the Ordinary Differential Equation.
    dy(1) = -a*y(2) + y(1) + t^2 + 6*t + b 
    dy(2) = b*y(1) - a*y(2) + 4*t + (a+b)*(1-t^2) 
endfunction 

function f = myDifferences ( x ) 
    // Returns the difference between the simulated differential 
    // equation and the experimental data.
    global MYDATA
    t = MYDATA.t
    y_exp = MYDATA.y_exp
    a = x(1) 
    b = x(2) 
    y0 = y_exp(1,:)
    t0 = 0
    y_calc=ode(y0',t0,t,list(myModel,a,b)) 
    diffmat = y_calc' - y_exp
    // Maxe a column vector
    f = diffmat(:)
endfunction 
//
// 1. Experimental data 
t = [0 1 2 3 4 5 6]'; 
y_exp(:,1) = [-1 2 11 26 47 74 107]'; 
y_exp(:,2) = [ 1 3 09 19 33 51 73]'; 
//
// 2. Store data for future use
global MYDATA;
MYDATA.t = t;
MYDATA.y_exp = y_exp;
// 3. Optimize
a = 0.1; 
b = 0.4; 
x0 = [a;b]; 
x0 = [0.1;0.4]; 
[fopt,xopt,gopt]=leastsq(myDifferences, x0)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->[fopt,xopt,gopt]=leastsq(myDifferences, x0)
 gopt  =
  - 1.060D-08  
  - 3.155D-09  
 xopt  =
    2.  
    3.  
 fopt  =
    1.804D-14  
\end{lstlisting}

The script presented in the section \ref{section-plotnonlinearleastsquares} produces 
the figure \ref{gif-bestparamfit}.

\begin{figure}
\includegraphics[width=15cm]{overview/nonlinearlsq_fitode}
\caption{Searching for the best parameters fitting experiments data associated with a set of 2 Ordinary Differential Equations.}
\label{gif-bestparamfit}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Genetic Algorithms}

The genetic algorithms module in Scilab provides the following functions:
\begin{itemize}
 \item optim\_ga : A flexible genetic algorithm
 \item optim\_moga : multi-objective genetic algorithm
 \item optim\_nsga : A multi-objective Niched Sharing Genetic Algorithm
 \item optim\_nsga2 : A multi-objective Niched Sharing Genetic Algorithm version 2
\end{itemize}

The following example is the minimization of the Rastrigin function defined for x in the range $[-1,1]^2$ by 
$z = x^2+y^2-\cos(12x)-\cos(18y)$.
This function has several local minima, but only one global minimum, that 
is $(x^\star,y^\star)=(0,0)$ associated with the value $f(x^\star,y^\star)=-2$.

We use a binary encoding of the input variables, which performs better in this case.

\lstset{language=scilabscript}
\begin{lstlisting}
// 1. Define the Rastrigin function.
function y = rastriginV ( x1 , x2  )
  // Vectorized function for contouring.
  y = x1.^2 + x2.^2-cos(12*x1)-cos(18*x2)
endfunction

function y = rastrigin ( x )
  // Non-vectorized function for optimization.
  y = rastriginV ( x(1) , x(2) )
endfunction

function y= rastriginBinary ( x )
  BinLen = 8
  lb = [-1 -1]';
  ub = [1 1]';
  tmp    = convert_to_float(x, BinLen, ub, lb)
  y      = rastrigin (tmp)
endfunction

// 2. Compute the optimum.
PopSize     = 100;
Proba_cross = 0.7;
Proba_mut   = 0.1;
NbGen       = 10;
Log         = %T;

ga_params = init_param();
ga_params = add_param(ga_params,"minbound",[-1 -1]');
ga_params = add_param(ga_params,"maxbound",[1 1]');
ga_params = add_param(ga_params,"dimension",2);
ga_params = add_param(ga_params,"binary_length",8);
ga_params = add_param(ga_params,"crossover_func",crossover_ga_binary);
ga_params = add_param(ga_params,"mutation_func",mutation_ga_binary);
ga_params = add_param(ga_params,"codage_func",coding_ga_binary);
ga_params = add_param(ga_params,"multi_cross",%T);


[pop_opt,fobj_pop_opt,pop_init,fobj_pop_init] = optim_ga(rastriginBinary, ..
    PopSize, NbGen, Proba_mut, Proba_cross, Log, ga_params);
\end{lstlisting}

The previous script produces the following output.

\lstset{language=scilabscript}
\begin{lstlisting}
-->optim_ga(rastriginBinary, PopSize, NbGen, ..
-->    Proba_mut, Proba_cross, Log, ga_params);
optim_ga: Initialization of the population
optim_ga: iter. 1 / 10 - min / max value = -1.942974 / 0.085538
optim_ga: iter. 2 / 10 - min / max value = -1.942974 / -0.492852
optim_ga: iter. 3 / 10 - min / max value = -1.942974 / -0.753347
optim_ga: iter. 4 / 10 - min / max value = -1.942974 / -0.841115
optim_ga: iter. 5 / 10 - min / max value = -1.942974 / -0.985001
optim_ga: iter. 6 / 10 - min / max value = -1.942974 / -1.094454
optim_ga: iter. 7 / 10 - min / max value = -1.942974 / -1.170877
optim_ga: iter. 8 / 10 - min / max value = -1.987407 / -1.255388
optim_ga: iter. 9 / 10 - min / max value = -1.987407 / -1.333186
optim_ga: iter. 10 / 10 - min / max value = -1.987407 / -1.450980
\end{lstlisting}

The script presented in the section \ref{section-plotgeneticalgorithms} 
produces the figure \ref{fig-optimga}.

\begin{figure}
\includegraphics[width=15cm]{overview/rastrigin_demo_binary}
\caption{Optimization of the Rastrigin function by the ''optim\_ga'' function.}
\label{fig-optimga}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Open-Source libraries}

In this section, we briefly present the open-source numerical optimization 
libraries which are used in Scilab.

\begin{itemize}
 \item optim is based on Modulopt: \url{http://www-rocq.inria.fr/~gilbert/modulopt/}, a collection of optimization solvers.
 \item lsqrsolve is based on Minpack: \url{http://www.netlib.org/minpack/}, a Fortran 77 code for solving nonlinear equations and nonlinear least squares problems.
 \item Quapro: Eduardo Casas Renteria, Cecilia Pola Mendez (Universidad De Cantabria), improved by Serge Steer (INRIA) and maintained by Allan Cornet,  Micha�l Baudin (Consortium Scilab - DIGITEO).
 \item qld : designed by M.J.D. Powell (1983) and modified by K. Schittkowski, with minor modifications by A. Tits and J.L. Zhou.
 \item qp\_solve, qpsolve : the Goldfarb-Idnani algorithm and the Quadprog package developed by Berwin A. Turlach.
 \item linpro, quapro : the algorithm developed by Eduardo Casas Renteria and Cecilia Pola Mendez.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Hot topics}

The following is a list of projects which are currently under development in optimization in Scilab.
\begin{itemize}
 \item Manage sparse linear optimization problems by updating Lipsol for Scilab.
 \item Improve the Scilab/Ipopt interface and the fmincon nonlinear optimization function.
 \item Improve the performance of the optim function L-BFGS by using the BLAS API.
 \item Provide Integer Programming solvers.
 \item Provide Design of Experiments and Response Surface tools.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Plot scripts}

\subsection{Plot of Genetic Algorithms}
\label{section-plotgeneticalgorithms}

The following script creates a plot for the Genetic Algorithms 
example presented in the previous example.

\lstset{language=scilabscript}
\begin{lstlisting}
// 3. Compute contouring values.
x1 = linspace(-1,1,100); 
x2 = linspace(-1,1,100); 
[XX1,XX2]=meshgrid(x1,x2);
Z = rastriginV ( XX1 , XX2  );

// 4. Plot the results.
scf();
drawlater;
// Plot initial population
subplot(2,1,1);
xset("fpf"," ");
contour ( x1 , x2 , Z' , 5 );
xtitle("Initial Population","X1","X2");
for i=1:length(pop_init)
  plot(pop_init(i)(1),pop_init(i)(2),"r.");
end
// Plot optimum population
subplot(2,1,2);
xset("fpf"," ");
contour ( x1 , x2 , Z' , 5 );
xtitle("Optimum Population","X1","X2");
for i=1:length(pop_opt)
  plot(pop_opt(i)(1),pop_opt(i)(2),"g.");
end
drawnow;
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Plot of Non Linear Least Squares}
\label{section-plotnonlinearleastsquares}

The following script creates a plot for the Nonlinear Least Squares 
example presented in the previous example.

\lstset{language=scilabscript}
\begin{lstlisting}
//
// 1. Before optimization
a=x0(1);
b=x0(2);
y0 = y_exp(1,:);
t0 = 0;
y_calc=ode(y0',t0,t,list(myModel,a,b));
scf();
subplot(2,2,1);
plot(t,y_calc(1,:),"r*");
plot(t,y_exp(:,1),"bo-");
legend(["Data","ODE"]);
xtitle("Before Optimization","t","Y1");
subplot(2,2,2);
plot(t,y_calc(2,:),"r*");
plot(t,y_exp(:,2),"bo-");
legend(["Data","ODE"]);
xtitle("Before Optimization","t","Y2");
//
// 2. After optimization
a=xopt(1);
b=xopt(2);
y0 = y_exp(1,:);
t0 = 0;
y_calc=ode(y0',t0,t,list(myModel,a,b));
subplot(2,2,3);
plot(t,y_calc(1,:),"r*");
plot(t,y_exp(:,1),"bo-");
legend(["Data","ODE"]);
xtitle("After Optimization","t","Y1");
subplot(2,2,4);
plot(t,y_calc(2,:),"r*");
plot(t,y_exp(:,2),"bo-");
legend(["Data","ODE"]);
xtitle("After Optimization","t","Y2");
//
// 3. Put transparent marks.
h = gcf();
for i = 1 : 4
   for j = 2 : 3
      h.children(i).children(j).children.mark_background=0;
   end
end
\end{lstlisting}
