% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\chapter{Genetic algorithms}
\section{Introduction}

Genetic algorithms are search algorithms based on the mechanics on
      natural selection and natural genetics \cite{Goldberg1989,Michalewicz96}.  Genetic algorithms have been
      introduced in Scilab v5 thanks to a work by Yann Collette \cite{yanncollettefree}. The
      solver is made of Scilab macros, which enables a high-level programming
      model for this optimization solver.

The problems solved by the current genetic algorithms in Scilab are the 
following :
\begin{itemize}
\item minimization of a cost function with bound constraints,
\item multi-objective non linear minimization with bound constraints.
\end{itemize}

Genetic algorithms are different from more normal optimization
and search procedures in four ways :
\begin{itemize}
\item GAs work with a coding of the parameter set, not the 
parameters themselves,
\item GAs search from a population of points, not a single point,
\item GAs use payoff (objective function) information, not derivativess
or other auxiliary knowledge,
\item GAs use probabilistic transition rules, not deterministic rules.
\end{itemize}

A simple genetic algorithm that yields good results in many 
practical problems is composed of three operators \cite{Goldberg1989} :
\begin{itemize}
\item reproduction, 
\item cross-over, 
\item mutation.
\end{itemize}

Many articles on this subject have been collected by Carlos A.
      Coello Coello on his website \cite{CoelloCoello}.
      A brief introduction to GAs is done in \cite{optimisationmultiobjectif}.

The GA macros are based on the "parameters" Scilab module for the
      management of the (many) optional parameters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Example}

In the current section, we give an example of the user of the GA algorithms.

The following is the definition of the Rastrigin function.

\lstset{language=scilabscript}
\begin{lstlisting}
function Res = min_bd_rastrigin()
Res = [-1 -1]';
endfunction
function Res = max_bd_rastrigin()
Res = [1 1]';
endfunction
function Res = opti_rastrigin()
Res = [0 0]';
endfunction
function y = rastrigin(x)
  y = x(1)^2+x(2)^2-cos(12*x(1))-cos(18*x(2));
endfunction
\end{lstlisting}

This cost function is then defined with the generic name "f".
Other algorithmic parameters, such as the size of the population, are 
defined in the following sample Scilab script.

\lstset{language=scilabscript}
\begin{lstlisting}
func = 'rastrigin';
deff('y=f(x)','y = '+func+'(x)');
PopSize     = 100;
Proba_cross = 0.7;
Proba_mut   = 0.1;
NbGen       = 10;
NbCouples   = 110;
Log         = %T;
nb_disp     = 10; 
pressure    = 0.05;
\end{lstlisting}

Genetic Algorithms require many settings, which are cleanly handled 
by the "parameters" module. This module provides the \scifun{nit\_param}
function, which returns a new, empty, set of parameters.
The \scifun{add\_param} function allows to set individual named parameters,
which are configure with key-value pairs.

\lstset{language=scilabscript}
\begin{lstlisting}
ga_params = init_param();
// Parameters to adapt to the shape of the optimization problem
ga_params = add_param(ga_params,'minbound',eval('min_bd_'+func+'()'));
ga_params = add_param(ga_params,'maxbound',eval('max_bd_'+func+'()'));
ga_params = add_param(ga_params,'dimension',2);
ga_params = add_param(ga_params,'beta',0);
ga_params = add_param(ga_params,'delta',0.1);
// Parameters to fine tune the Genetic algorithm. 
ga_params = add_param(ga_params,'init_func',init_ga_default);
ga_params = add_param(ga_params,'crossover_func',crossover_ga_default);
ga_params = add_param(ga_params,'mutation_func',mutation_ga_default);
ga_params = add_param(ga_params,'codage_func',coding_ga_identity);
ga_params = add_param(ga_params,'selection_func',selection_ga_elitist);
ga_params = add_param(ga_params,'nb_couples',NbCouples);
ga_params = add_param(ga_params,'pressure',pressure);
\end{lstlisting}

The \scifun{optim\_ga} function search a population solution of a single-objective
problem with bound constraints. 

\lstset{language=scilabscript}
\begin{lstlisting}
[pop_opt, fobj_pop_opt, pop_init, fobj_pop_init] = ...
   optim_ga(f, PopSize, NbGen, Proba_mut, Proba_cross, Log, ga_params);
\end{lstlisting}

The following are the messages which are displayed in the Scilab console :

\lstset{language=scilabscript}
\begin{lstlisting}
optim_ga: Initialization of the population
optim_ga: iteration 1 / 10 - min / max value found = -1.682413 / 0.081632
optim_ga: iteration 2 / 10 - min / max value found = -1.984184 / -0.853613
optim_ga: iteration 3 / 10 - min / max value found = -1.984184 / -1.314217
optim_ga: iteration 4 / 10 - min / max value found = -1.984543 / -1.513463
optim_ga: iteration 5 / 10 - min / max value found = -1.998183 / -1.691332
optim_ga: iteration 6 / 10 - min / max value found = -1.999551 / -1.871632
optim_ga: iteration 7 / 10 - min / max value found = -1.999977 / -1.980356
optim_ga: iteration 8 / 10 - min / max value found = -1.999979 / -1.994628
optim_ga: iteration 9 / 10 - min / max value found = -1.999989 / -1.998123
optim_ga: iteration 10 / 10 - min / max value found = -1.999989 / -1.999534
\end{lstlisting}

The initial and final populations for this simulation are shown in \ref{optim_ga_rastrigin}.

\begin{figure}
\includegraphics[width=15cm]{geneticalgorithms/ga_rastrigin_contour.png}
\caption{Optimum of the Rastrigin function -- Initial population is in red, Optimum population is accumulated on the blue dot}
\label{optim_ga_rastrigin}
\end{figure}

The following script is a loop over the optimum individuals of the population.

\lstset{language=scilabscript}
\begin{lstlisting}
printf('Genetic Algorithm: %d points from pop_opt\n', nb_disp);
for i=1:nb_disp 
printf('Individual %d: x(1) = %f x(2) = %f -> f = %f\n', ...
  i, pop_opt(i)(1), pop_opt(i)(2), fobj_pop_opt(i));
end
\end{lstlisting}

The previous script make the following lines appear in the Scilab console.

\lstset{language=scilabscript}
\begin{lstlisting}
Individual 1: x(1) = -0.000101 x(2) = 0.000252 -> f = -1.999989
Individual 2: x(1) = -0.000118 x(2) = 0.000268 -> f = -1.999987
Individual 3: x(1) = 0.000034 x(2) = -0.000335 -> f = -1.999982
Individual 4: x(1) = -0.000497 x(2) = -0.000136 -> f = -1.999979
Individual 5: x(1) = 0.000215 x(2) = -0.000351 -> f = -1.999977
Individual 6: x(1) = -0.000519 x(2) = -0.000197 -> f = -1.999974
Individual 7: x(1) = 0.000188 x(2) = -0.000409 -> f = -1.999970
Individual 8: x(1) = -0.000193 x(2) = -0.000427 -> f = -1.999968
Individual 9: x(1) = 0.000558 x(2) = 0.000260 -> f = -1.999966
Individual 10: x(1) = 0.000235 x(2) = -0.000442 -> f = -1.999964
\end{lstlisting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Support functions}

In this section, we analyze the GA services to configure a GA
computation.

\subsection{Coding}

The following is the list of coding functions available in Scilab's GA :
\begin{itemize}
\item \scifun{coding\_ga\_binary} : A function which performs conversion between binary and continuous representation
\item \scifun{coding\_ga\_identity} : A "no-operation" conversion function
\end{itemize}

The user may configure the GA parameters so that the algorithm uses a customized coding function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Cross-over}

The crossover function is used when mates have been computed, based on the Wheel 
algorithm : the crossover algorithm is a loop over the couples, which modifies 
both elements of each couple.

The following is the list of crossover functions available in Scilab :
\begin{itemize}
\item \scifun{crossover\_ga\_default} : A crossover function for continuous variable functions. 
\item \scifun{crossover\_ga\_binary} : A crossover function for binary code
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Selection}

The selection function is used in the loop over the generations, when the 
new population is computed by processing a selection over the 
individuals.

The following is the list of selection functions available in Scilab :
\begin{itemize}
\item \scifun{selection\_ga\_random} : A function which performs a random selection of individuals. 
We select pop\_size individuals in the set of parents and childs individuals at random.
\item \scifun{selection\_ga\_elitist} : An 'elitist' selection function. 
We select the best individuals in the set of parents and childs individuals.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Initialization}

The initialization function returns a population as a list made of "pop\_size" individuals.
The Scilab macro \scifun{init\_ga\_default} computes this population by performing
a randomized discretization of the domain defined by the bounds as minimum and 
maximum arrays.
This randomization is based on the Scilab primitive \scifun{rand}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Solvers}

In this section, we analyze the 4 GA solvers which are available in Scilab :
\begin{itemize}
\item \scifun{optim\_ga} : flexible genetic algorithm 
\item \scifun{optim\_moga} : multi-objective genetic algorithm 
\item \scifun{optim\_nsga} : multi-objective Niched Sharing Genetic Algorithm 
\item \scifun{optim\_nsga2} : multi-objective Niched Sharing Genetic Algorithm version 2 
\end{itemize}

While \scifun{optim\_ga} is designed for one objective, the 3 other solvers are 
designed for multi-objective optimization.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{optim\_ga}

The Scilab macro \scifun{optim\_ga} implements a Genetic Algorithm to 
find the solution of an optimization problem with one objective function and 
bound constraints.

The following is an overview of the steps in the GA algorithm.
\begin{itemize}
\item processing of input arguments

In the case where the input cost function is a list, one defines the 
"hidden" function \scifun{\_ga\_f} which computes the cost function.
If the input cost function is a regular Scilab function, the "hidden" function \scifun{\_ga\_f}
simply encapsulate the input function.

\item initialization 

One computes the initial population with the \scifun{init\_func} callback
function (the default value for \scifun{init\_func} is \scifun{init\_ga\_default})

\item coding 

One encodes the initial population with the \scifun{codage\_func} callback
function (default~: \scifun{coding\_ga\_identity})
\item evolutionary algorithm as a loop over the generations

\item decoding 

One decodes the optimum population back to the original
variable system
\end{itemize}

The loop over the generation is made of the following steps.
\begin{itemize}
\item reproduction : two list of children populations are computed, based on a randomized Wheel,
\item crossover : the two populations are processed through the \scifun{crossover\_func}
callback function (default~: \scifun{crossover\_ga\_default})
\item mutation : the two populations are processed throught the \scifun{mutation\_func}
callback function (default~: \scifun{mutation\_ga\_default})
\item computation of cost functions : the \scifun{\_ga\_f} function is called to compute the 
fitness for all individuals of the two populations
\item selection : the new generation is computed by processing the two populations through 
the \scifun{selection\_func} callback function (default~: \scifun{selection\_ga\_elitist})
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{optim\_moga, pareto\_filter}

The \scifun{optim\_moga} function is a multi-objective genetic algorithm. 
The method is based on \cite{FonsecaFleming1993}.

The function \scifun{pareto\_filter} extracts non dominated solution from a set.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{optim\_nsga}

The \scifun{optim\_nsga} function is a multi-objective Niched Sharing Genetic Algorithm. 
The method is based on \cite{Srinivas94multiobjectiveoptimization}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{optim\_nsga2}

The function \scifun{optim\_nsga2} is a multi-objective Niched Sharing Genetic Algorithm. 
The method is based on \cite{Deb00afast}.


