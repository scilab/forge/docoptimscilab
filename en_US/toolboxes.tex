% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\chapter{Scilab Optimization Toolboxes}


Several Scilab toolboxes are designed to solve optimization problems. 
In this chapter, we present some of them, including the CUTEr module, 
which provides a collection of various types of optimization problems.
We also present the Unconstrained Optimization Problem toolbox, 
which is a Scilab port of the Mor\'e, Garbow and Hillstrom package.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{CUTEr}
CUTEr is a versatile testing environment for optimization and
linear algebra solvers \cite{CUTErHSL}.  
This toolbox is a scilab port by Serge Steer and Bruno Durand of the original Matlab toolbox.

A typical use start from problem selection using the scilab function
\scifun{sifselect}. This gives a vector of problem names corresponding to
selection criteria \cite{CUTErTestProblemSetwww}. The available problems are located in the sif
directory.

The \scifun{sifbuild} function can then be used to generate the fortran codes
associated to a given problem, to compile them and dynamically link it
to Scilab. This will create a set of problem relative functions, for example, 
ufn or ugr. This functions can be called to compute the objective
function or its gradient at a given point.

The \scifun{sifoptim} function automatically applies the optim function to a 
selected problem.

A Fortran compiler is mandatory to build problems.

This toolbox contains the following parts.
\begin{itemize}
\item Problem database 

 A set of testing problems coded in "Standard Input Format" (SIF) is included in the sif/ sub-directory. This set comes from www.numerical.rl.ac.uk/cute/mastsif.html. 
 The Scilab function \scifun{sifselect} can be used to select some of this problems according to objective 
 function properties, contraints properties and regularity properties 
 
\item SIF format decoder 

The Scilab function \scifun{sifdecode} can be used to generate the Fortran codes 
associated to a given problem, while the Scilab function \scifun{buildprob} compiles and dynamically links these fortran code with Scilab 
\item problem relative functions 

 The execution of the function buildprob adds a set of functions to Scilab. 
 The first one is \scifun{usetup} for unconstrained or bounded problems or \scifun{csetup} 
 for problems with general contraints. 
 These functions are to be called before any of the following to initialize the problem relative data (only one problem can be run at a time). 
 The other functions allow to compute the objective, the gradient, the hessian values, ... of the problem at a given point 
 (see \scifun{ufn}, \scifun{ugr}, \scifun{udh}, ... 
 for unconstrained or bounded problems or \scifun{cfn}, \scifun{cgr}, \scifun{cdh}, ... 
 for problems with general contraints) 
\item CUTEr and \scifun{optim} 
The Scilab function \scifun{optim} can be used together with CUTEr 
using either the external function \scifun{ucost} or the driver function \scifun{sifoptim}.
\end{itemize}

The following is a list of references for the CUTEr toolbox : 
\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=812}{CUTEr toolbox on Scilab Toolbox center}
\item \href{http://hsl.rl.ac.uk/cuter-www/}{CUTEr website}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The Unconstrained Optimization Problem Toolbox}

The Unconstrained Optimization Problem Toolbox 
provides 35 unconstrained optimization problems.

The goal of this toolbox is to provide unconstrained optimization problems 
in order to test optimization algorithms.

The More, Garbow and Hillstrom collection of test functions \cite{MoreGarbowHillstrom1981} 
is widely used in testing unconstrained optimization software. 
The problems available in this collection are presented in the figure 
\ref{optisci-fig-uncprfuns}. All the problems are defined 
as a sum of squares of functions. The dimension of the space is denoted by n, while 
the number of functions is denoted by m. 

\begin{figure}
\begin{center}
\begin{tabular}{lllll}
No. & file & n & m & Name \\
\hline
1 & ROSE    & 2 & 2    & Rosenbrock \\
2 & FROTH   & 2 & 2    & Freudenstein and Roth \\
3 & BADSCP  & 2 & 2    & Powell Badly Scaled \\
4 & BADSCB  & 2 & 3    & Brown Badly Scaled \\
5 & BEALE   & 2 & 3    & Beale \\
6 & JENSAM  & 2 & (10) & Jennrich and Sampson \\
7 & HELIX   & 3 & 3    & Helical Valley \\
8 & BARD    & 3 & 15   & Bard \\
9 & GAUSS   & 3 & 15   & Gaussian \\
10 & MEYER  & 3 & 16   & Meyer \\
11 & GULF   & 3 & (10) & Gulf Research and Development \\
12 & BOX    & 3 & (10) & Box 3-Dimensional \\
13 & SING   & 4 & 4    & Powell Singular \\
14 & WOOD   & 4 & 6    & Wood \\
15 & KOWOSB & 4 & 11   & Kowalik and Osborne \\
16 & BD     & 4 & (20) & Brown and Dennis \\
17 & OSB1   & 5 & 33   & Osborne 1 \\
18 & BIGGS  & 6 & (13) & Biggs EXP6 \\
19 & OSB2   & 11 & 65  & Osborne 2 \\
20 & WATSON & (10) & 31 & Watson \\
21 & ROSEX  & (20) & (20) & Extended Rosenbrock \\
22 & SINGX  & (12) & (12) & Extended Powell Singular \\
23 & PEN1   & (8) & (9)   & Penalty I \\
24 & PEN2   & (8) & (16)  & Penalty II \\
25 & VARDIM & (10) & (12) & Variably Dimensioned \\
26 & TRIG   & (10) & (10) & Trigonometric \\
27 & ALMOST & (10) & (10) & Brown Almost Linear \\
28 & BV     & (8) & (8)   & Discrete Boundary Value \\
29 & IE     & (8) & (8)   & Discrete Integral Equation \\
30 & TRID   & (12) & (12) & Broyden Tridiagonal \\
31 & BAND   & (15) & (15) & Broyden Banded \\
32 & LIN    & (10) & (15) & Linear - Full Rank \\
33 & LIN1   & (10) & (15) & Linear - Rank 1 \\
34 & LIN0   & (10) & (15) & Linear - Rank 1 with Zero Cols and Rows \\
35 & CHEB   & (10) & (10) & Chebyquad \\
\end{tabular}
\caption{Problems available in the Unconstrained Optimization Problems toolbox}
\label{optisci-fig-uncprfuns}
\end{center}
\end{figure}

It provides the function value, the gradient, the function vector, the Jacobian and 
provides the Hessian matrix for 18 problems.
It provides the starting point for each problem, the optimum function value 
and the optimum point x for many problems.
Additionnally, it provides finite difference routines for the gradient, the Jacobian and the
Hessian matrix.
The functions are based on macros based functions : no compiler is required,
which is an advantage over the CUTEr toolbox.
Finally, all function values, gradients, Jacobians and Hessians are tested.

This toolbox is available in ATOMS :
\begin{center}
\url{http://atoms.scilab.org/toolboxes/uncprb}
\end{center}
and is manage under Scilab's Forge :
\begin{center}
\url{http://forge.scilab.org/index.php/p/uncprb}
\end{center}

To install it, type the following statement in Scilab v5.2 (or better).
\lstset{language=scilabscript}
\begin{lstlisting}
atomsInstall("uncprb")
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The FSQP Interface}

The FSQP toolbox provides an interface for the Feasible Sequential Quadratic Programming library. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Overview of the FSQP module}


This toolbox is designed for non-linear optimization with equality and inequality constraints. 
The authors of FSQP are Craig T. Lawrence, Jian L. Zhou, and Andre L. Tits 
from the Institute for Systems Research and Electrical Engineering Department, 
University of Maryland (the copyright notice includes the dates 1993-1998).

FSQP is a commercial product but is provided by the authors, free of charge, for an academic use.
In order to get the source code, the user has to make the request directly to AEM Design :
\begin{center}
\href{http://www.aemdesign.com/}{FSQP website}
\end{center}
The source code is \verb|cfsqp.c|.

The FSQP module is available on the former Scilab Toolbox Center 
\begin{center}
\href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=1219}{FSQP on Scilab Toolbox center}
\end{center}

The Scilab module provides the interface to the FSQP library.
In order to build the module, we must copy the \verb|cfsqp.c| file into the 
\verb|fsqp/sci_gateway/c| directory.
Then we execute the \verb|fsqp/builder.sce| script. 
At this point, a compiler is necessary.

The interface to FSQP has been written by F. Delebecque (INRIA) 
and J.P. Chancelier (ENPC), with modifications by S. Steer (INRIA).
The 2009 version has been revised by S. Steer in order to take into 
account for the version 5 of Scilab.

They are older versions of this module, namely \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=1042}{FSQP 2007-09-24},
and \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=1042}{FSQP 2000-02-01}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The \scifun{fsqp} and \scifun{srfsqp} functions}
These functions can solve problems with non-linear inequality and 
equality constraints and non differentiable min-max objective functions.

The \scifun{fsqp} and \scifun{srfsqp} functions are based on sequential quadratic programming 
\cite{Lawrence96nonlinearequality,PanierTits1989,zhou1996,PanierTits1993,BonnansPanier1992,ZhouTits1992,Lawrence98feasiblesequential}.
These can solve problems with non-linear inequality and equality constraints as well as
non-differentiable min-max objective functions.
 
The \scifun{fsqp} function solves the optimization problem: 
    \begin{eqnarray}
      \left\{\begin{array}{l}
          \underset{x\in \RR^n} {\min}~  \underset{i \in \{1,2,\ldots, n_f\}}{max(f_i(x))}  \\
          \text{s.t.}\\
          x_{inf} \leq x \leq x_{sup}\\
          g_j(x) \leq 0 \text{,}~   j=1,2,\ldots,n_i\\
          C x-d \leq 0     \\
          h_k(x) = 0 \text{,}~   k=1,2,\ldots,n_e \\
          A x = b     \\
          \end{array}\right.
    \end{eqnarray}
where the objective functions $f_i~:~\RR^n~\rightarrow~\RR$ are $C^1$,
the inequality functions $g_j~:~\RR^n~\rightarrow~\RR$ are $C^1$
and the equality functions $h_k~:~\RR^n~\rightarrow~\RR$ are $C^1$.
The matrix $C$ is a real $n \times n_{li}$ matrix and the matrix $A$ is a 
real $n \times n_{le}$ matrix.

    If the number of functions is zero, i.e. if $n_f=0$, then the solver returns a point 
    that verify the set of constraints.

    The \scifun{srfsqp} function solves sequentially related optimization problem:
   \begin{eqnarray}
      \left\{\begin{array}{l} 
          \underset{x\in \RR^n}{\min}~ \underset{i\in \{1,2,\ldots, n_f\}}{max}~ \underset{\omega \in \Omega^{f_i}}{max}~ f_i(x,\omega) \\
          \text{s.t.}\\
          x_{inf} \leq x \leq x_{sup}\\
          g_j(x,\xi) \leq 0,~ \forall \xi \in \Xi^{g_j}\text{,}~   j=1,2,\ldots,n_i\\
          h_k(x) = 0,~ \text{,}~   k=1,2,\ldots,n_e \\
        \end{array}\right.
    \end{eqnarray}
    where the objective functions $f_i~: \RR^n \times \Omega^{f_i}
    \rightarrow \RR$ are $C^1$ with respect to $x$,
the inequality functions $g_j~: \RR^n \times \Xi^{g_j} \rightarrow \RR$ are $C^1$ w.r.t. $x$
and the equality functions $h_k~: \RR^n \rightarrow \RR$ are smooth.

The internal structure of FSQP is presented in figures 
\ref{fmc-CFSQP-structure1} and \ref{fmc-CFSQP-structure2}.

\begin{figure}
\begin{center}

\begin{tabular}{|ll|}
\hline
make\_iv() & \\
make\_dv() & \\
make\_dm() & \\
free\_iv() & \\
free\_dv() & \\
free\_dm() & \\
convert() & \\
ql0001c() & \\
diagnl() & \\
error() & \\
estlam() & \\
colvec() & \\
scaprd() & \\
small() & \\
fuscmp() & \\
indexs() & \\
matrcp() & \\
\hline
\end{tabular}

\end{center}
\caption{Structure of CFSQP (part \#1).}
\label{fmc-CFSQP-structure1}
\end{figure}

\begin{figure}
\begin{center}

\begin{tabular}{|ll|}
\hline
matrvc() & \\
nullvc() & \\
resign() & \\
sbout1() & \\
sbout2() & \\
shift() & \\
slope() & \\
element() & \\
cfsqp1() & \\
check() & \\
initpt() & \\
dir() & \\
step1() & \\
hessian() & \\
out() & \\
update\_omega() & \\
dealloc() & \\
dqp() & \\
di1() & \\
dealloc1() & \\
\hline
\end{tabular}

\end{center}
\caption{Structure of CFSQP (part \#2).}
\label{fmc-CFSQP-structure2}
\end{figure}

A partial call tree of CFSQP is presented in figure \ref{fmc-CFSQP-calltree}.

\begin{figure}
\begin{center}
\includegraphics[width=6cm]{cfsqp_structure.pdf}
\end{center}
\caption{Call tree of CFSQP.}
\label{fmc-CFSQP-calltree}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{An example}

In the following example, we solve a nonlinear optimization problem with 2 variables and 2 constraints. 
The first constraint is nonlinear while the second constraint is linear. 
We provide the analytical gradient to \scifun{fsqp}.
\lstset{language=scilabscript}
\begin{lstlisting}
function fj=obj32(j,x)
  fj=(x(1)+3*x(1)+x(2))^2+4*(x(1)-x(2))^2,
endfunction

function gj=cntr32(j,x)
  select j
  case 1
    gj=x(1)^3-6*x(2)-4*x(3)+3;
  case 2
    gj=1-sum(x);
  end
endfunction

function gradf=grob32(j,x)
  fa=2*(x(1)+3*x(2)+x(3));
  fb=8*(x(1)-x(2));
  gradf=[fa+fb;3*fa-fb;fa];
endfunction

function gradgj=grcn32(j,x)
  select j
  case 1
    gradgj=[3*x(1)^2;-6;-4];
  case 2
    gradgj=[-1;-1;-1];
  end
endfunction

x0=[0.1;0.7;0.2];

nf=1; 
nineqn=1; 
nineq=1; 
neqn=0; 
neq=1; 
modefsqp=100; 
miter=500; 
iprint=1;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

bigbnd=1.e10; 
eps=1.e-8; 
epsneq=0.e0; 
udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];

x=fsqp(x0,ipar,rpar,[bl,bu],obj32,cntr32,grob32,grcn32)
\end{lstlisting}

The previous script produces the following output.
\lstset{language=scilabscript}
\begin{lstlisting}
-->x=fsqp(x0,ipar,rpar,[bl,bu],obj32,cntr32,grob32,grcn32)


  CFSQP Version 2.5d (Released February 1998) 
          Copyright (c) 1993 --- 1998       
           C.T. Lawrence, J.L. Zhou         
                and A.L. Tits               
             All Rights Reserved            

The given initial point is feasible for inequality
         constraints and linear equality constraints:
                     	  1.00000000000000e-001
 			  7.00000000000000e-001
 			  2.00000000000000e-001
 objectives
 			  2.65000000000000e+000
 constraints
 			 -1.99900000000000e+000
 			  0.00000000000000e+000

 iteration                                    3
 inform                                       0
 x                   	 -9.86076131526265e-032
 			  0.00000000000000e+000
 			  1.00000000000000e+000
 objectives
 			  1.94469227433161e-061
 constraints
 			 -1.00000000000000e+000
 			  0.00000000000000e+000
 d0norm              	  1.39452223873684e-031
 ktnorm              	  1.06098265851897e-030
 ncallf                                       3
 ncallg                                       5


Normal termination: You have obtained a solution !!

 x  =
 
  - 9.861D-32  
    0.         
    1.         
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Other toolboxes}

In this section, we present the some significant optimization toolboxes, 
including the interface to FSQP, to CONMIN, to LIPSOL, to LPSOLVE and to NEWUOA.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Interface to \textbf{CONMIN}}

An interface to the NASTRAN / NASA CONMIN optimization program by Yann Collette.
CONMIN can solve a nonlinear objective problem with nonlinear constraints. 
CONMIN uses a two-step limited memory quasi-Newton-like Conjugate Gradient. 
The CONMIN optimization method is currently used in NASTRAN (a professionnal
finite element tool) and the optimization part of NASTRAN (the CONMIN tool).
The CONMIN fortran program has been written by G. Vanderplaats (1973).

\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=1086}{CONMIN on Scilab Toolbox center}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Differential Evolution}

A random search of a global minimum by Helmut Jarausch. 
This toolbox is based on a Rainer-Storn algorithm.
\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=907}{Differential Evolution on Scilab Toolbox center}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{IPOPT interface}
An interface for \textsc{Ipopt}, which is 
based on an interior point method which can handle equality and 
inequality nonlinear constraints. This solver can handle large scale 
optimization problems. As open source software, the source code for Ipopt is provided without charge. 
You are free to use it, also for commercial purposes.
This Scilab-Ipopt interface was based on the Matlab Mex
Interface developed by Claas Michalik and Steinar Hauan. This version only works on linux, scons and
Scilab >=4.0. Tested with gcc 4.0.3. Modifications to Scilab Interface made by
Edson Cordeiro do Valle.

\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=839}{Ipopt on Scilab Toolbox center}
\item \href{https://projects.coin-or.org/Ipopt}{Ipopt website}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Interface to \textbf{LIPSOL}}
A sparse linear problems solver with interior points method by H. Rubio Scola. 
LIPSOL can minimize a linear objective with linear constraints and bound constraints.
It is based on a primal-dual interior point method, 
which uses sparse-matrix data-structure to solve large, sparse, symmetric positive definite linear systems. 
LIPSOL is written by Yin Zhang . The original Matlab-based code
has been adapted to Scilab by H. Rubio Scola (University of Rosario,
Argentina). It is distributed freely under the terms of the GPL.
LIPSOL also uses the ORNL sparse Cholesky solver version 0.3 written by
Esmond Ng and Barry Peyton by H. Rubio Scola.
\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=813}{LIPSOL on Scilab Toolbox center}
\item \href{http://www.caam.rice.edu/~zhang/lipsol/}{LIPSOL website}
\item \href{http://www.caam.rice.edu/~zhang/lipsol/v0.4/UserGuide.pdf}{LIPSOL User's Guide}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{LPSOLVE}
An interface to lp\_solve. The lp\_solve library is a free 
mixed integer/binary linear programming solver with full source, examples and manuals.
lp\_solve is under LGPL, the GNU lesser general public license. 
lp\_solve uses the 'Simplex' algorithm and sparse matrix methods for pure LP problems.
\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=160}{LPSOLVE toolbox on Scilab Toolbox center}
\item \href{http://lpsolve.sourceforge.net/}{lp\_solve solver on Sourceforge}
\item \href{http://www.geocities.com/lpsolve/}{lp\_solve on Geocities}
\item \href{http://groups.yahoo.com/group/lp_solve}{lp\_solve Yahoo Group}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{NEWUOA}

NEWUOA is a software developped by M.J.D. Powell for unconstrained optimization
without derivatives. The NEWUOA seeks the least value of a function  F(x)  (x is a vector of
dimension  n ) when  F(x)  can be calculated for any vector of variables  x .
The algorithm is iterative, a quadratic model being required at the beginning of
each iteration, which is used in a trust region procedure for adjusting the
variables. When the quadratic model is revised, the new model interpolates  F at
 m points, the value m=2n+1 being recommended.

\begin{itemize}
\item \href{http://www.scilab.org/contrib/index_contrib.php?page=displayContribution&fileID=294}{NEWUOA toolbox on Scilab Toolbox center}
\item \href{http://www.inrialpes.fr/bipop/people/guilbert/newuoa/newuoa.html}{NEWUOA at INRIA Alpes}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{SciCobyla}
The SciCobyla optimization method is a derivative free non linear 
constrained optimization method.
It is available on ATOMS : \url{http://atoms.scilab.org/toolboxes/scicobyla}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{AMPL Toolbox}

The AMPL toolbox allows you to load into Scilab .nl files created using AMPL.
Once loaded into Scilab, you can use them like any other objective functions,
get derivatives, constraints, etc.
You can use Scilab optimization tools to find some solutions.
You must compile ampl .mod files into .nl files before using this toolbox to
load the file into Scilab. 
It is available on ATOMS : \url{http://atoms.scilab.org/toolboxes/ampl_toolbox}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The Optkelley Toolbox}

These Scilab files are implementations of the algorithms from the book
"Iterative Methods for Optimization", by C.T. Kelley \cite{OtimizationKelley1999}.

The following is an overview of the functions available :
\begin{itemize}
\item \scifun{optkelley\_bfgswopt} : Steepest descent/bfgs with polynomial line search.
\item \scifun{optkelley\_cgtrust} : Steihaug Newton-CG-Trust region algorithm.
\item \scifun{optkelley\_diffhess} : Compute a forward difference Hessian.
\item \scifun{optkelley\_dirdero} : Finite difference directional derivative.
\item \scifun{optkelley\_gaussn} : Damped Gauss-Newton with Armijo rule.
\item \scifun{optkelley\_gradproj} : Gradient projection with Armijo rule, simple linesearch
\item \scifun{optkelley\_hooke} : Hooke-Jeeves optimization.
\item \scifun{optkelley\_imfil} : Unconstrained implicit filtering.
\item \scifun{optkelley\_levmar} : Levenberg-Marquardt.
\item \scifun{optkelley\_mds} : Multidirectional search.
\item \scifun{optkelley\_nelder} : Nelder-Mead optimizer, No tie-breaking rule other than Scilab's gsort.
\item \scifun{optkelley\_ntrust} : Dogleg trust region.
\item \scifun{optkelley\_polyline} : Polynomial line search.
\item \scifun{optkelley\_polymod} : Cubic/quadratic polynomial linesearch.
\item \scifun{optkelley\_projbfgs} : Projected BFGS with Armijo rule, simple linesearch
\item \scifun{optkelley\_simpgrad} : Simplex gradient.
\item \scifun{optkelley\_steep} : Steepest descent with Armijo rule.
\end{itemize}

The optimization codes have the calling convention
\lstset{language=scilabscript}
\begin{lstlisting}
[f,g] = objective(x) 
\end{lstlisting}
returns both the objective function value f and the gradient vector g. I 
expect \scivar{g} to be a column vector. The Nelder-Mead, Hooke-Jeeves, Implicit
Filtering, and MDS codes do not ask for a gradient.

The authors of this toolbox are C. T. Kelley, Yann Collette, Michael Baudin.

To install it, type the following statement in Scilab v5.2 (or better).
\lstset{language=scilabscript}
\begin{lstlisting}
atomsInstall("optkelley")
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Fminuit}

Fminuit is an optimization and chi-square fitting program for Matlab, 
Octave and Scilab, based on the MINUIT minimization engine. 
It is available at 
\begin{center}
\url{http://www.fis.unipr.it/~giuseppe.allodi/Fminuit}
\end{center}
Minuit is a library of Fortran 77 subroutines developed at CERN. 
Fminuit runs under Matlab, as well as its clones (Octave, Scilab), as a 
dynamic executable, i.e. it is invoked as a Matlab/Scilab function, with 
a calling sequence similar (although not identical) to that of former 
Matlab's fmins (whence the name). 

The Fminuit program is copyright © 1996-2010 by Giuseppe Allodi. 
The underlying MINUIT package was written by F. James at CERN, Geneva 
(Switzerland). 
The copyright is owned by CERN. 

Fminuit Is Free Software under the terms of The Gnu General Public License.



