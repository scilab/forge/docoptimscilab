% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\chapter{Simulated Annealing}

In this document, we describe the Simulated Annealing optimization
    methods, a new feature available in Scilab v5 .

\section{Introduction}

    Simulated annealing (SA) is a generic probabilistic meta-algorithm
    for the global optimization problem, namely locating a good approximation
    to the global optimum of a given function in a large search space. It is
    often used when the search space is discrete (e.g., all tours that visit a
    given set of cities) \cite{simulatedannealingwikipedia}.

    Genetic algorithms have been introduced in Scilab v5 thanks to the
    work by Yann Collette \cite{yanncollettefree}.
    
The current Simulated Annealing solver aims at finding the solution of 
$$
\min_x f(x)
$$
with bounds constraints and with $f:\mathbb{R}^n \rightarrow \mathbb{R}$ the cost function.
    
    Reference books on the subject are \cite{LaarhovenAarts1987,Laarhoven1988,davis1987}.
  
\section{Overview}

The solver is made of Scilab macros, which enables a high-level programming model for this optimization solver. 
The GA macros are based on the "parameters" Scilab module for the management of the (many) optional parameters. 

To use the SA algorithm, one must perform the following steps :
    \begin{itemize}
      \item configure the parameters with calls to "init\_param" and "add\_param" especially,
	\begin{itemize}
      \item the neighbor function,
      \item the acceptance function,
      \item the temperature law,
    \end{itemize}
      \item compute an initial temperature with a call to "compute\_initial\_temp"
      \item find an optimum by using the "optim\_sa" solver
    \end{itemize}

\section{Example}

The following example is extracted from the SA examples. 
The Rastrigin functin is used as an example of a dimension 2 problem
because it has many local optima but only one global optimum.

\lstset{language=scilabscript}
\begin{lstlisting}
//
// Rastrigin function
//
function Res = min_bd_rastrigin()
Res = [-1 -1]';
endfunction
function Res = max_bd_rastrigin()
Res = [1 1]';
endfunction
function Res = opti_rastrigin()
Res = [0 0]';
endfunction
function y = rastrigin(x)
  y = x(1)^2+x(2)^2-cos(12*x(1))-cos(18*x(2));
endfunction
//
// Set parameters
//
func = 'rastrigin';
Proba_start = 0.8;
It_intern = 1000;
It_extern = 30;
It_Pre    = 100;
Min = eval('min_bd_'+func+'()');
Max = eval('max_bd_'+func+'()');
x0  = (Max - Min).*rand(size(Min,1),size(Min,2)) + Min;
deff('y=f(x)','y='+func+'(x)');
//
// Simulated Annealing with default parameters
//
  printf('SA: geometrical decrease temperature law\n');

  sa_params = init_param();
  sa_params = add_param(sa_params,'min_delta',-0.1*(Max-Min));
  sa_params = add_param(sa_params,'max_delta', 0.1*(Max-Min));
  sa_params = add_param(sa_params,'neigh_func', neigh_func_default); 
  sa_params = add_param(sa_params,'accept_func', accept_func_default); 
  sa_params = add_param(sa_params,'temp_law', temp_law_default); 
  sa_params = add_param(sa_params,'min_bound',Min);
  sa_params = add_param(sa_params,'max_bound',Max);

  T0 = compute_initial_temp(x0, f, Proba_start, It_Pre, sa_params);
  printf('Initial temperature T0 = %f\n', T0);

  [x_opt, f_opt, sa_mean_list, sa_var_list, temp_list] = ...
     optim_sa(x0, f, It_extern, It_intern, T0, Log = %T, sa_params);

  printf('optimal solution:\n'); disp(x_opt);
  printf('value of the objective function = %f\n', f_opt);

  scf();
  drawlater;
  subplot(2,1,1);
  xtitle('Geometrical annealing','Iteration','Mean / Variance');
  t = 1:length(sa_mean_list);
  plot(t,sa_mean_list,'r',t,sa_var_list,'g');
  legend(['Mean','Variance']);
  subplot(2,1,2);
  xtitle('Temperature evolution','Iteration','Temperature');
  for i=1:length(t)-1
    plot([t(i) t(i+1)], [temp_list(i) temp_list(i)],'k-');
  end
  drawnow;
\end{lstlisting}

After some time, the following messages appear in the Scilab console.

\lstset{language=scilabscript}
\begin{lstlisting}
optimal solution:
  - 0.0006975  
  - 0.0000935  
value of the objective function = -1.999963
\end{lstlisting}

The figure \ref{sa_convergence} presents the evolution of Mean, Variance and Temperature depending on the iteration.

\begin{figure}
\includegraphics[width=10cm]{simulatedannealing/sa_convergence.png}
\caption{Convergence of the simulated annealing algorithm}
\label{sa_convergence}
\end{figure}

  \section{Neighbor functions}

In the simulated annealing algorithm, a neighbour function is used in order to explore 
the domain \cite{optimisationmultiobjectif}.

The prototype of a neighborhood function is the following :

\lstset{language=scilabscript}
\begin{lstlisting}
function x_neigh = neigh_func_default(x_current, T, param)
\end{lstlisting}
where:
\begin{itemize}
\item x\_current represents the current point, 
\item T represents the current temperature, 
\item param is a list of parameters.
\end{itemize}


The following is a list of the neighbour functions available in the SA context :
    \begin{itemize}
      \item \scifun{neigh\_func\_default} : SA function which computes a neighbor of a given point. For example, for a continuous vector, a neighbor will be produced by adding some noise to each component of the vector. For a binary string, a neighbor will be produced by changing one bit from 0 to 1 or from 1 to 0.
      \item \scifun{neigh\_func\_csa} : The classical neighborhood relationship for the simulated annealing. The neighbors distribution is a gaussian distribution which is more and more peaked as the temperature decrease.
      \item \scifun{neigh\_func\_fsa} : The Fast Simulated Annealing neghborhood relationship. The corresponding distribution is a Cauchy distribution which is more and more peaked as the temperature decreases.
      \item \scifun{neigh\_func\_vfsa} : The Very Fast Simulated Annealing neighborhood relationship. This distribution is more and more peaked as the temperature decreases.
    \end{itemize}
  
  \section{Acceptance functions}

There exist several kind of simulated annealing optimization methods: 
\begin{itemize}
\item the Fast Simulated Annealing, 
\item the simulated annealing based on metropolis-hasting acceptance function, 
\item etc...
\end{itemize}

To implement these various simulated annealing optimization methods, you only need to change the acceptance function. 
For common optimization, you need not to change the default acceptance function. 

The following is a list of acceptance functions available in Scilab SAs :
\begin{itemize}
\item \scifun{accept\_func\_default} : is the default acceptance function, based on the exponential function 
$$
level = exp\left(-\frac{F_{neigh} - F_{current} }{T}\right)
$$
\item \scifun{accept\_func\_vfsa} : is the Very Fast Simulated Annealing function, defined by :
$$
Level = \frac{1 }{ 1 + exp\left(-\frac{F_{current} - F_{neigh}}{T}\right)}
$$
\end{itemize}

  \section{Temperature laws}

In the simulated annealing algorithm, a temperature law is used in a statistical 
criteria for the update of the optimum \cite{optimisationmultiobjectif}.
If the new (neighbor) point improves the current optimum, the update is done with the new point replacing the old optimum. 
If not, the update may still be processed, provided that a statistical criteria is satisfied. The statistical law decreases while the iterations are processed. 

There are 5 temperature laws available in the SA context :
\begin{itemize}
\item \scifun{temp\_law\_default} : A SA function which computes the temperature of the next temperature stage
\item \scifun{temp\_law\_csa} : The classical temperature decrease law, the one for which the convergence of the simulated annealing has been proven
\item \scifun{temp\_law\_fsa} : The Szu and Hartley Fast simulated annealing
\item \scifun{temp\_law\_huang} : The Huang temperature decrease law for the simulated annealing
\item \scifun{temp\_law\_vfsa} : This function implements the Very Fast Simulated Annealing from L. Ingber
\end{itemize}


  \section{optim\_sa}
The \scifun{optim\_sa} macro implements the simulated annealing solver.
It allows to find the solution of an minimization problem with bound constraints.

It is based on an iterative update of two points :
\begin{itemize}
\item the current point is updated by taking into account 
the neighbour function and the acceptance criterium,
\item the best point is the point which achieved the minimum of the 
objective function over the iterations.
\end{itemize}
While the current point is used internally to explore the domain, 
only the best point is returned as the algorithm output.

The algorithm is based on the following steps, which include a main, external loop over the 
temperature decreases, and an internal loop.

\begin{itemize}
\item processing of input arguments,
\item initialization,
\item loop over the number of temperature decreases.
\end{itemize}

For each iteration over the temperature decreases, the following steps 
are processed.
\begin{itemize}
\item loop over internal iterations, with constant temperature,
\item if history is required by user, store the temperature, the x iterates, the values of f,
\item update the temperature with the temperature law.
\end{itemize}

The internal loop allows to explore the domain and is based on the neighbour 
function. It is based on the following steps.

\begin{itemize}
\item compute a neighbour of the current point,
\item compute the objective function for that neighbour
\item if the objective decreases or if the acceptance criterium is true, 
then overwrite the current point with the neighbour
\item if the cost of the best point is greater than the cost of the current
point, overwrite the best point by the current point.
\end{itemize}

%\addcontentsline{toc}{section}{Bibliography}
%\bibliographystyle{plain}
%\bibliography{scilab.optimization}






